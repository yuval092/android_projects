﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ISharedPreferences sp;
        EditText first;
        EditText last;
        Button save;
        TextView result;
        RadioButton b1;
        RadioButton b2;
        RadioButton b3;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            // creating shared preferences object
            sp = this.GetSharedPreferences("data", FileCreationMode.Private);

            this.first = (EditText)FindViewById(Resource.Id.etFname);
            this.last = (EditText)FindViewById(Resource.Id.etLname);
            this.save = (Button)FindViewById(Resource.Id.btnSubmit);
            this.result = (TextView)FindViewById(Resource.Id.tvDisplay);
            this.b1 = (RadioButton)FindViewById(Resource.Id.id10);
            this.b2 = (RadioButton)FindViewById(Resource.Id.id30);
            this.b3 = (RadioButton)FindViewById(Resource.Id.id50);

            // putting current values of first and last name into text view.
            string Fname = sp.GetString("Fname", null);
            string Lname = sp.GetString("Lname", null);
            int Font = sp.GetInt("Font", 0);

            this.result.TextSize = Font;
            this.result.Text = Fname + " " + Lname;

            this.save.Click += Save_Click;
        }

        private void Save_Click(object sender, System.EventArgs e)
        {
            var editor = this.sp.Edit();

            editor.PutString("Fname", this.first.Text);
            editor.PutString("Lname", this.last.Text);

            if (this.b1.Checked)
                editor.PutInt("Font", 10);
            else if (this.b2.Checked)
                editor.PutInt("Font", 30);
            else if (this.b3.Checked)
                editor.PutInt("Font", 50);
            editor.Commit();

            string Fname = sp.GetString("Fname", null);
            string Lname = sp.GetString("Lname", null);
            int Font = sp.GetInt("Font", 0);

            this.result.TextSize = Font;
            this.result.Text = Fname + " " + Lname;
        }
    }
}