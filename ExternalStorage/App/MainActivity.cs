﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Graphics;
using Android.Content;
using System;
using System.IO;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView iv;
        Button bt1;
        TextView tv1;
        Bitmap bitmap;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            bt1 = (Button)FindViewById(Resource.Id.bt1);
            tv1 = (TextView)FindViewById(Resource.Id.tv1);
            iv = (ImageView)FindViewById(Resource.Id.iv);

            bt1.Click += Bt1_Click;
        }

        private void Bt1_Click(object sender, System.EventArgs e)
        {
            Intent i = new Intent(Android.Provider.MediaStore.ActionImageCapture);
            StartActivityForResult(i, 0);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 0)
            {
                if (resultCode == Result.Ok)
                {
                    this.bitmap = (Android.Graphics.Bitmap)data.Extras.Get("data");
                    this.iv.SetImageBitmap(bitmap);
                    saveImageToExternalStorage(bitmap);
                }
            }

        }

        private void saveImageToExternalStorage(Bitmap finalbitmap)
        {
            string root = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures).ToString();
            Java.IO.File myDir = new Java.IO.File(root + "/saved_images");
            myDir.Mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.Next(n);
            string fname = "Image" + n + ".jpg";
            Java.IO.File file = new Java.IO.File(myDir, fname);
            if (file.Exists())
                file.Delete();
            try
            {
                String path = System.IO.Path.Combine(myDir.AbsolutePath, fname);
                var fs = new FileStream(path, FileMode.Create);
                if (fs != null)
                {
                    finalbitmap.Compress(Bitmap.CompressFormat.Png, 90, fs);
                    tv1.Text = myDir.AbsolutePath;
                }
                fs.Flush();
                fs.Close();

            }
            catch (Exception e)
            {
                tv1.Text = e.ToString();
                Toast.MakeText(this, e.ToString(), ToastLength.Long).Show();
            }
        }
    }
}