﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace Ex2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button emailBtn, broswerBtn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.emailBtn = (Button)FindViewById(Resource.Id.email);
            this.broswerBtn = (Button)FindViewById(Resource.Id.broswer);

            this.emailBtn.Click += EmailBtn_Click;
            this.broswerBtn.Click += BroswerBtn_Click;
        }

        private void BroswerBtn_Click(object sender, System.EventArgs e)
        {
            Intent i = new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://www.facebook.com"));
            StartActivity(i);
        }

        private void EmailBtn_Click(object sender, System.EventArgs e)
        {
            string[] emails = { "yuval.farkash8@gmail.com" };

            Intent i = new Intent(Intent.ActionSend);
            i.SetType("text/plain");
            i.PutExtra(Intent.ExtraEmail, emails);
            i.PutExtra(Intent.ExtraSubject, "Subject...");
            i.PutExtra(Intent.ExtraText, "Body...");
            StartActivity(i);
        }
    }
}