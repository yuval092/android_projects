﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    [Activity(Label = "Activity2")]
    [IntentFilter(new[] { "com.targil.actions.TARGIL" }, Categories = new[] { Intent.CategoryDefault })]

    public class Activity2 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layout2);

        }
    }
}