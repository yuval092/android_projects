﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView iv;
        LinearLayout main;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            LoadPics1(5);
        }

        public void LoadPics1(int n)
        {
            this.main = (LinearLayout)FindViewById(Resource.Id.l1);
            int imageKey;

            for(int i = 1; i <= n; ++i)
            {
                this.iv = new ImageView(this);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
                //lp.SetMargins(100, 20, 30, 40);
                iv.LayoutParameters = lp;

                imageKey = Resources.GetIdentifier("d" + i, "drawable", this.PackageName);
                iv.SetImageResource(imageKey);
                main.AddView(iv);
            }
        }
    }
}