﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views.Animations;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8;
        Animation anim1, anim2, anim3, anim4, anim5, anim6,anim7,anim8,anim9,anim10;
        ImageView img, img2;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btn1 = (Button)FindViewById(Resource.Id.btn1);
            this.btn2 = (Button)FindViewById(Resource.Id.btn2);
            this.btn3 = (Button)FindViewById(Resource.Id.btn3);
            this.btn4 = (Button)FindViewById(Resource.Id.btn4);
            this.btn5 = (Button)FindViewById(Resource.Id.btn5);
            this.btn6 = (Button)FindViewById(Resource.Id.btn6);
            this.btn7 = (Button)FindViewById(Resource.Id.btn7);
            this.btn8 = (Button)FindViewById(Resource.Id.btn8);

            this.img = (ImageView)FindViewById(Resource.Id.lion);
            this.img2 = (ImageView)FindViewById(Resource.Id.lion2);

            this.anim1 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim1);
            this.anim2 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim2);
            this.anim3 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim3);
            this.anim4 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim4);
            this.anim5 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim5);
            this.anim6 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim6);
            this.anim7 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim7);
            this.anim8 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim8);
            this.anim9 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim9);
            this.anim10 = AnimationUtils.LoadAnimation(this, Resource.Animation.anim10);

            this.btn1.Click += Btn_Click;
            this.btn2.Click += Btn2_Click;
            this.btn3.Click += Btn3_Click;
            this.btn4.Click += Btn4_Click;
            this.btn5.Click += Btn5_Click;
            this.btn6.Click += Btn6_Click;
            this.btn7.Click += Btn7_Click;
            this.btn8.Click += Btn8_Click;
        }

        private void Btn8_Click(object sender, System.EventArgs e)
        {
            this.img2.StartAnimation(this.anim8);
        }

        private void Btn7_Click(object sender, System.EventArgs e)
        {
            this.btn7.StartAnimation(this.anim7);
        }

        private void Btn6_Click(object sender, System.EventArgs e)
        {
            this.btn6.StartAnimation(this.anim6);
        }

        private void Btn5_Click(object sender, System.EventArgs e)
        {
            this.btn5.StartAnimation(this.anim5);
        }

        private void Btn4_Click(object sender, System.EventArgs e)
        {
            this.img.StartAnimation(this.anim4);
        }

        private void Btn3_Click(object sender, System.EventArgs e)
        {
            this.btn3.StartAnimation(this.anim3);
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            this.btn2.StartAnimation(this.anim2);
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
            this.btn1.StartAnimation(this.anim1);
        }
    }
}