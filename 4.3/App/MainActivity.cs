﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        LinearLayout main;
        CheckBox mailMsg;
        EditText name;
        EditText mail;
        Button submit;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            // attaching the linear layout in axml to object main.
            this.main = (LinearLayout)FindViewById(Resource.Id.l1);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

            // creating EditText object for the full name of the user.
            this.name = new EditText(this);
            this.name.Hint = "Full Name: ";
            this.name.LayoutParameters = lp;

            // create CheckBox object.
            this.mailMsg = new CheckBox(this);
            this.mailMsg.Text = "Like to be updated?";

            this.main.AddView(this.name);
            this.main.AddView(this.mailMsg);

            this.mailMsg.Click += MailMsg_Click;
        }

        private void Submit_Click(object sender, System.EventArgs e)
        {
            string s = "Welcome ";
            if (this.name != null && this.name.Text != "")
                s += this.name.Text;
            if (this.mail != null && this.mail.Text != "")
                s += " " + this.mail.Text;

            Toast.MakeText(this, s, ToastLength.Long).Show();
        }

        private void MailMsg_Click(object sender, System.EventArgs e)
        {
            if(this.mailMsg.Checked)
            {
                this.mail = new EditText(this);
                this.mail.Hint = "Mail: ";
                this.main.AddView(this.mail);

                // submit button
                this.submit = new Button(this);
                this.submit.Text = "Submit";
                this.main.AddView(this.submit);

                this.submit.Click += Submit_Click;
            }
            else
            {
                this.main.RemoveView(this.mail);
                this.main.RemoveView(this.submit);
            }
        }
    }
}