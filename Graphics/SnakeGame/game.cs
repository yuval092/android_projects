﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Threading;

namespace SnakeGame
{
    class game : View
    {
        private const int BLOCK_SIZE = 55;
        private const int SPEED = 8;

        Paint rect, circ, head;
        Context context;

        private List<int[]> snakeList;
        private int snakeLength;

        private int lead_x, lead_y;
        private int lead_x_change, lead_y_change;

        private int rand_apple_x, rand_apple_y, apple_size;
        private bool to_random;
        Bitmap apple;

        private bool gameOver;

        public game(Context context) : base(context)
        {
            this.context = context;

            this.head = new Paint();
            this.head.Color = Color.Blue;
            this.rect = new Paint();
            this.rect.Color = Color.Green;
            this.circ = new Paint();
            this.circ.Color = Color.Red;

            this.snakeList = new List<int[]>();
            this.snakeLength = 1;
            this.lead_x = this.lead_y = 100;
            this.lead_x_change = BLOCK_SIZE;
            this.lead_y_change = 0;

            this.to_random = true;
            this.apple = BitmapFactory.DecodeResource(this.context.Resources, Resource.Drawable.apple);

            this.gameOver = false;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            if (gameOver)
                return;

            this.apple_size = this.apple.GetScaledWidth(canvas);
            GenerateRandApple(canvas.Width, canvas.Height);

            moveBetweenBorders(canvas.Width, canvas.Height);

            this.lead_x += this.lead_x_change;
            this.lead_y += this.lead_y_change;

            // display apple
            canvas.DrawBitmap(this.apple, this.rand_apple_x, rand_apple_y, null);

            // handle the snake's length
            this.snakeList.Add(new int[] { this.lead_x, this.lead_y });
            if (this.snakeList.Count > this.snakeLength)
                this.snakeList.RemoveAt(0);

            checkForCollision();

            // draw snake (first the head and then the rest of the body).
            int[] headPos = this.snakeList[snakeList.Count - 1];
            canvas.DrawRect(headPos[0], headPos[1], headPos[0] + BLOCK_SIZE, headPos[1] + BLOCK_SIZE, this.head);
            foreach (int[] cube in this.snakeList.Take(snakeList.Count - 1))
            {
                canvas.DrawRect(cube[0], cube[1], cube[0] + BLOCK_SIZE, cube[1] + BLOCK_SIZE, this.rect);
            }

            isAppleTouched(canvas.Width, canvas.Height);

            // in order to control frames per second.
            Thread.Sleep(50);

            Invalidate();
        }


        // generate a new apple in a random location
        private void GenerateRandApple(int w, int h)
        {
            if (this.to_random)
            {
                this.rand_apple_x = new Random().Next(100, w - 100);
                this.rand_apple_y = new Random().Next(100, h - 100);
            }

            this.to_random = false;
        }


        // move between borders
        private void moveBetweenBorders(int w, int h)
        {
            if (lead_x >= w && this.lead_x_change == BLOCK_SIZE && this.lead_y_change == 0)
                this.lead_x = 0;
            else if (lead_x <= 0 && this.lead_x_change == -BLOCK_SIZE && this.lead_y_change == 0)
                this.lead_x = w;
            else if (lead_y >= h && this.lead_x_change == 0 && this.lead_y_change == BLOCK_SIZE)
                this.lead_y = 0;
            else if (lead_y <= 0 && this.lead_x_change == 0 && this.lead_y_change == -BLOCK_SIZE)
                this.lead_y = h;
        }


        // check if the snake collided with himself. if he did, game over.
        private void checkForCollision()
        {
            int[] headPos = snakeList[snakeList.Count - 1];
            for (int i = 0; i < this.snakeList.Count - 1; ++i)
                if (this.snakeList[i][0] == headPos[0] && this.snakeList[i][1] == headPos[1])
                    gameOver = true;
        }


        // if the snake touched the apple, handle it.
        private void isAppleTouched(int w, int h)
        {
            if (lead_x >= rand_apple_x && lead_x <= rand_apple_x + this.apple_size ||
                lead_x + BLOCK_SIZE >= rand_apple_x && lead_x + BLOCK_SIZE <= rand_apple_x + this.apple_size)
            {
                if (lead_y > rand_apple_y && lead_y < rand_apple_y + this.apple_size + 1 ||
                    lead_y + BLOCK_SIZE > rand_apple_y && lead_y + BLOCK_SIZE < rand_apple_y + this.apple_size + 1)
                {
                    this.to_random = true;
                    GenerateRandApple(w, h);
                    snakeLength++;
                }
            }
        }


        // change the direction of the snake when the player clicks the screen.
        public override bool OnTouchEvent(MotionEvent e)
        {
            if (MotionEventActions.Down == e.Action)
            {
                if (this.lead_x_change == BLOCK_SIZE && this.lead_y_change == 0)        // change RIGHT to DOWN
                {
                    this.lead_x_change = 0;
                    this.lead_y_change = BLOCK_SIZE;
                }
                else if (this.lead_x_change == 0 && this.lead_y_change == BLOCK_SIZE)        // change DOWN to LEFT
                {
                    this.lead_x_change = -BLOCK_SIZE;
                    this.lead_y_change = 0;
                }
                else if (this.lead_x_change == -BLOCK_SIZE && this.lead_y_change == 0)        // change LEFT to TOP
                {
                    this.lead_x_change = 0;
                    this.lead_y_change = -BLOCK_SIZE;
                }
                else if (this.lead_x_change == 0 && this.lead_y_change == -BLOCK_SIZE)        // change TOP to RIGHT
                {
                    this.lead_x_change = BLOCK_SIZE;
                    this.lead_y_change = 0;
                }
            }

            return true;
        }
    }
}