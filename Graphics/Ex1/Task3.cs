﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex1
{
    class Task3 : View
    {
        private const int SIZE = 75;

        Paint circle;
        Paint rect;
        Context context;

        private float x1, y1;
        private float deltaX1, deltaY1;
        private bool isRight1;

        private float x2, y2;
        private float deltaX2, deltaY2;
        private bool isRight2;


        public Task3(Context context) : base(context)
        {
            this.context = context;

            this.circle = new Paint();
            this.rect = new Paint();
            this.circle.Color = Color.Blue;
            this.rect.Color = Color.Red;

            this.isRight1 = this.isRight2 = true;
            this.deltaX1 = this.deltaX2 = 10;
            this.deltaY1 = this.deltaY2 = 5;
            this.x1 = 100;
            this.y1 = 100;
            this.x2 = 873;
            this.y2 = 528;

        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            canvas.DrawColor(Color.LightGray);

            if (isRight1)
                canvas.DrawCircle(this.x1, this.y1, SIZE, this.circle);
            else
                canvas.DrawRect(this.x1, this.y1, this.x1 + SIZE, this.y1 + SIZE, this.rect);

            if (isRight2)
                canvas.DrawCircle(this.x2, this.y2, SIZE, this.circle);
            else
                canvas.DrawRect(this.x2, this.y2, this.x2 + SIZE, this.y2 + SIZE, this.rect);


            this.x1 += this.deltaX1;
            this.y1 += this.deltaY1;

            this.deltaX1 = this.x1 < SIZE || this.x1 > canvas.Width - SIZE ? -this.deltaX1 : this.deltaX1;
            this.deltaY1 = this.y1 < SIZE || this.y1 > canvas.Height - SIZE ? -this.deltaY1 : this.deltaY1;
            this.isRight1 = this.deltaX1 < 0 ? false : true;

            this.x2 += this.deltaX2;
            this.y2 += this.deltaY2;

            this.deltaX2 = this.x2 < SIZE || this.x2 > canvas.Width - SIZE ? -this.deltaX2 : this.deltaX2;
            this.deltaY2 = this.y2 < SIZE || this.y2 > canvas.Height - SIZE ? -this.deltaY2 : this.deltaY2;
            this.isRight2 = this.deltaX2 < 0 ? false : true;

            Invalidate();
        }
    }
}