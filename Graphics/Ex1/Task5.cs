﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    class Task5 : View
    {
        Paint p;
        Bitmap bitmap;
        Context context;

        private int x;
        private int deltaX;

        public Task5(Context context) : base(context)
        {
            this.context = context;
            this.bitmap = BitmapFactory.DecodeResource(this.context.Resources, Resource.Drawable.pic);
            this.p = new Paint();
            this.p.Color = Color.Blue;

            this.x = 0;
            this.deltaX = 10;
        }

        protected override void OnDraw(Canvas canvas)
        {
            canvas.DrawBitmap(this.bitmap, this.x, 20, null);

            if (x >= canvas.Width - 100)
                deltaX = -10;
            else if (x <= 0)
                deltaX = 10;

            x += deltaX;

            Invalidate();
        }
    }
}