﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex1
{
    class Task2 : View
    {
        private const int RADIUS = 75;

        Paint circle1;
        Paint circle2;
        Context context;

        private float x1, y1;
        private float deltaX1, deltaY1;

        private float x2, y2;
        private float deltaX2, deltaY2;


        public Task2(Context context) : base(context)
        {
            this.context = context;

            this.circle1 = new Paint();
            this.circle2 = new Paint();
            this.circle1.Color = Color.Red;
            this.circle2.Color = Color.Blue;
            this.circle1.StrokeWidth = this.circle2.StrokeWidth = 12;

            this.deltaX1 = this.deltaX2 = 10;
            this.deltaY1 = this.deltaY2 = 5;
            this.x1 = 100;
            this.y1 = 100;
            this.x2 = 873;
            this.y2 = 528;

        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            canvas.DrawColor(Color.LightGray);
            canvas.DrawCircle(this.x1, this.y1, RADIUS, this.circle1);
            canvas.DrawCircle(this.x2, this.y2, RADIUS, this.circle2);

            this.x1 += this.deltaX1;
            this.y1 += this.deltaY1;

            this.deltaX1 = this.x1 < RADIUS || this.x1 > canvas.Width - RADIUS ? -this.deltaX1 : this.deltaX1;
            this.deltaY1 = this.y1 < RADIUS || this.y1 > canvas.Height - RADIUS ? -this.deltaY1 : this.deltaY1;

            this.x2 += this.deltaX2;
            this.y2 += this.deltaY2;

            this.deltaX2 = this.x2 < RADIUS || this.x2 > canvas.Width - RADIUS ? -this.deltaX2 : this.deltaX2;
            this.deltaY2 = this.y2 < RADIUS || this.y2 > canvas.Height - RADIUS ? -this.deltaY2 : this.deltaY2;


            Invalidate();
        }
    }
}