﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex1
{
    class Task1 : View
    {
        Paint circle1;
        Paint circle2;
        Context context;


        public Task1(Context context) : base(context)
        {
            this.context = context;

            this.circle1 = new Paint();
            this.circle2 = new Paint();
            this.circle1.Color = Color.Red;
            this.circle2.Color = Color.Blue;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            canvas.DrawCircle(200, 200, 150, this.circle1);
            canvas.DrawCircle(873, 528, 50, this.circle2);

            Invalidate();
        }
    }
}