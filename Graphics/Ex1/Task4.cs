﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex1
{
    class Task4 : View
    {
        Paint p;
        Context context;
        private int exNum, b, a;

        public Task4(Context context) : base(context)
        {
            this.context = context;
            this.p = new Paint();
            this.p.Color = Color.Blue;
            this.a = 200;
            this.b = 4;
            this.exNum = 4;
        }

        protected override void OnDraw(Canvas canvas)
        {
            if (exNum == 1)
            {
                canvas.DrawRect(0, 0, a, b, p);
                canvas.DrawRect(0, canvas.Height - b, a, canvas.Height, p);
                canvas.DrawRect(canvas.Width - a, 0, canvas.Width, b, p);
                canvas.DrawRect(canvas.Width - a, canvas.Height - b, canvas.Width, canvas.Height, p);
            }
            else if (exNum == 2)
            {
                canvas.DrawRect(0, 0, canvas.Width, b, p);
                canvas.DrawRect(0, canvas.Height - b, canvas.Width, canvas.Height, p);
                p.TextSize = 70;
                p.TextAlign = Paint.Align.Center;
                canvas.DrawText("welcome to israel", canvas.Width / 2, canvas.Height / 2, p);
            }
            else if (exNum == 3)
            {
                Paint p2 = new Paint();
                p2.Color = Color.Black;
                p2.StrokeWidth = 15;
                p2.SetStyle(Paint.Style.Stroke);
                p.Color = Color.Red;

                for (int i = 0; i < 6; i++)
                {
                    canvas.DrawRect(i * a, 0, (i + 1) * a, b, p);
                    canvas.DrawRect(i * a, 0, (i + 1) * a, b, p2);
                }
            }
            else if (exNum == 4)
            {
                Paint p2 = new Paint();
                p2.Color = Color.Black;
                p2.StrokeWidth = 10;
                p2.SetStyle(Paint.Style.Stroke);
                Paint p3 = new Paint();
                p3.Color = Color.Yellow;
                p3.TextSize = 70;
                p.Color = Color.Red;

                float cx = a / 2;
                for (float i = 1; i <= b; i++)
                {
                    canvas.DrawCircle(cx, (a / 2) + 1, a / 2, p);
                    canvas.DrawCircle(cx, (a / 2) + 1, a / 2, p2);
                    canvas.DrawText(i.ToString(), cx - 20, (a / 2) + 20, p3);
                    cx += a;
                }
            }
            else if (exNum == 5)
            {
                float atemp = a;
                float wtemp = 0;
                for (int i = 0; i < b; i++)
                {
                    canvas.DrawRect(wtemp, wtemp, wtemp + atemp, wtemp + atemp, p);
                    wtemp += atemp;
                    atemp = atemp * 2 / 3;
                }
            }

            Invalidate();
        }
    }
}