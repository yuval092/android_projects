﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    class Task6 : View
    {
        Paint p;
        Bitmap bitmap;
        Context context;

        private int x, y;

        public Task6(Context context) : base(context)
        {
            this.context = context;
            this.bitmap = BitmapFactory.DecodeResource(this.context.Resources, Resource.Drawable.pic);
            this.p = new Paint();
            this.p.Color = Color.Blue;

            this.x = 0;
            this.y = 20;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            canvas.DrawBitmap(this.bitmap, this.x, this.y, null);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (MotionEventActions.Move == e.Action)
            {
                this.x = (int)e.GetX();
                this.y = (int)e.GetY();
                Invalidate();
            }

            return true;
        }
    }
}