﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex2
{
    class Coin
    {
        bourdGame bourd;
        float x, y, r, lastX, lastY;
        Paint p;

        public Coin(bourdGame bourd, float x, float y, float r, float lastX, float lastY)
        {
            this.bourd = bourd;
            this.x = x;
            this.y = y;
            this.r = r;
            this.lastX = lastX;
            this.lastY = lastY;
            this.p = new Paint();
            this.p.Color = Color.Green;
        }

        public void Draw(Canvas canvas)
        {
            canvas.DrawCircle(x, y, r, p);
        }

        public bool didUserTouch(float x, float y)
        {
            if (x > this.x - this.r && x < this.x + this.r && y > this.y - this.r && y < this.y + this.r)
                return true;
            return false;
        }

        public void setX(float x)
        {
            this.x = x;
        }

        public void setY(float y)
        {
            this.y = y;
        }

        public void setR(float r)
        {
            this.r = r;
        }
    }
}