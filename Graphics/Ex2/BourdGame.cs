﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex2
{
    class bourdGame : View
    {
        bool isCoin;
        Coin coin;
        Square[,] squers;
        Context context;

        public bourdGame(Context c) : base(c)
        {
            this.context = c;
            this.squers = new Square[6, 6];
            this.isCoin = false;
            this.coin = new Coin(this, 0, 0, 0, 0, 10);
        }

        public void DrawBoard(Canvas canvas)
        {
            int x = 0, y = 0, h = canvas.Width / 6, w = canvas.Width / 6;
            Color color;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (i % 2 == 0)
                        if (j % 2 == 0)
                            color = Color.White;
                        else
                            color = Color.Black;
                    else
                        if (j % 2 == 0)
                        color = Color.Black;
                    else
                        color = Color.White;
                    this.squers[i, j] = new Square(x, y, w, h, color, this);
                    this.squers[i, j].Draw(canvas);
                    x = x + w;
                }
                y = y + h;
                x = 0;

            }
        }

        public void DrawCoin(Canvas canvas)
        {
            if (!this.isCoin)
            {
                float w = canvas.Width / 6;
                float h = canvas.Width / 6;
                coin.setX(w / 2);
                coin.setY(h / 2);
                coin.setR(canvas.Width / 24);
                this.isCoin = true;
            }
            coin.Draw(canvas);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            DrawBoard(canvas);
            DrawCoin(canvas);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (e.Action == MotionEventActions.Move)
            {
                if (coin.didUserTouch(e.GetX(), e.GetY()))
                {
                    coin.setX(e.GetX());
                    coin.setY(e.GetY());
                }
            }
            else
               if (e.Action == MotionEventActions.Up)
            {
                coin.setX(e.GetX());
                coin.setY(e.GetY());
                Invalidate();
            }
            return true;


        }
    }
}