﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace Ex2
{
    class Square
    {
        float x, y, h, w;
        Color color;
        Paint p;
        bourdGame board;

        public Square(float x, float y, float h, float w, Color c, bourdGame Bg)
        {
            this.x = x;
            this.y = y;
            this.h = h;
            this.w = w;
            this.color = c;
            this.p = new Paint();
            this.board = Bg;
        }

        public void Draw(Canvas canvas)
        {
            this.p.Color = this.color;
            canvas.DrawRect(x, y, x + w, y + h, p);
        }

        public bool isXandY(float x, float y)
        {
            if (x - this.x < this.w && y - this.y < this.h)
                return true;
            return false;
        }
    }
}