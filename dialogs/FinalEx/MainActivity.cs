﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using Android;
using AlertDialog = Android.App.AlertDialog;
using System;

namespace FinalEx
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText first, last, city;
        Button dateBtn, genderBtn, saveBtn;
        DateTime date;
        DatePickerDialog datepick;
        Dialog dialog;
        AlertDialog.Builder builder;
        ISharedPreferences sp;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            this.sp = this.GetSharedPreferences("varsData", FileCreationMode.Private);
            this.first = (EditText)FindViewById(Resource.Id.firstName);
            this.last = (EditText)FindViewById(Resource.Id.lastName);
            this.city = (EditText)FindViewById(Resource.Id.city);
            this.dateBtn = (Button)FindViewById(Resource.Id.dateBtn);
            this.genderBtn = (Button)FindViewById(Resource.Id.genderBtn);
            this.saveBtn = (Button)FindViewById(Resource.Id.saveBtn);

            uploadData();


            this.dateBtn.Click += DateBtn_Click;
            this.genderBtn.Click += GenderBtn_Click;
            this.saveBtn.Click += SaveBtn_Click;
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            this.builder = new AlertDialog.Builder(this);
            this.builder.SetTitle("Save Dialog");
            this.builder.SetMessage("Are you sure you want to save?");
            this.builder.SetCancelable(true);
            builder.SetPositiveButton("Yes", OkAction);
            builder.SetNegativeButton("No", CancelAction);
            AlertDialog dialog = this.builder.Create();

            dialog.Show();
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            Toast.MakeText(this, "No confirm", ToastLength.Long).Show();
        }

        // save to shared prefrences
        private void OkAction(object sender, DialogClickEventArgs e)
        {
            ISharedPreferencesEditor editor = this.sp.Edit();
            editor.PutString("firstName", this.first.Text);
            editor.PutString("lastName", this.last.Text);
            editor.PutString("city", this.city.Text);
            editor.PutString("date", this.dateBtn.Text);
            editor.PutString("gender", this.genderBtn.Text);
            editor.Commit();

            Toast.MakeText(this, "Yes confirm", ToastLength.Long).Show();
        }

        public void uploadData()
        {
            this.first.Text = this.sp.GetString("firstName", "");
            this.last.Text = this.sp.GetString("lastName", "");
            this.city.Text = this.sp.GetString("city", "");
            this.dateBtn.Text = this.sp.GetString("date", "select date");
            this.genderBtn.Text = this.sp.GetString("gender", "male/ female");
        }

        private void GenderBtn_Click(object sender, EventArgs e)
        {
            CreateGenderDialog();
        }

        public void CreateGenderDialog()
        {
            this.dialog = new Dialog(this);
            this.dialog.SetContentView(Resource.Layout.gender);
            this.dialog.SetTitle("gender");
            this.dialog.SetCancelable(true);

            RadioButton rad1 = (RadioButton)this.dialog.FindViewById(Resource.Id.male);
            RadioButton rad2 = (RadioButton)this.dialog.FindViewById(Resource.Id.female);
            Button submit = (Button)this.dialog.FindViewById(Resource.Id.submitBtn);

            rad1.Click += Rad1_Click;
            rad2.Click += Rad1_Click;

            submit.Click += Submit_Click;
            this.dialog.Show();
        }

        private void Rad1_Click(object sender, EventArgs e)
        {
            RadioButton btn = (RadioButton)sender;            
            this.genderBtn.Text = btn.Text;
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            this.dialog.Dismiss();
        }

        private void DateBtn_Click(object sender, System.EventArgs e)
        {
            this.date = DateTime.Today;
            this.datepick = new DatePickerDialog(this, onDatePick, date.Year, date.Month - 1, date.Day);

            datepick.Show();
        }

        private void onDatePick(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            string str = e.Date.ToLongDateString();
            Toast.MakeText(this, str, ToastLength.Long).Show();
            this.dateBtn.Text = str;
        }
    }
}