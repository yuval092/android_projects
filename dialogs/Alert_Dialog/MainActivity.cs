﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using AlertDialog = Android.App.AlertDialog;
using System;

namespace Alert_Dialog
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btnAlert;
        AlertDialog.Builder builder;

        Button btnProgress;
        ProgressDialog p;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btnAlert = (Button)FindViewById(Resource.Id.btn1);
            this.btnProgress = (Button)FindViewById(Resource.Id.btn2);

            this.btnAlert.Click += BtnAlert_Click;
            this.btnProgress.Click += BtnProgress_Click;
        }

        private void BtnProgress_Click(object sender, EventArgs e)
        {
            this.p = ProgressDialog.Show(this, "Message", "Loading please wait...", true);
            this.p.SetCancelable(true);
            this.p.SetProgressStyle(ProgressDialogStyle.Horizontal);
            this.p.SetMessage("Loading...");

            this.p.Show();
        }

        private void BtnAlert_Click(object sender, System.EventArgs e)
        {
            this.builder = new AlertDialog.Builder(this);
            this.builder.SetTitle("Yuval");
            this.builder.SetMessage("This is a message");
            this.builder.SetCancelable(true);
            builder.SetPositiveButton("Yes", OkAction);
            builder.SetNegativeButton("Cancel", CancelAction);
            AlertDialog dialog = this.builder.Create();

            dialog.Show();
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            Toast.MakeText(this, "cancel confirm", ToastLength.Long).Show();
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            Toast.MakeText(this, "ok confirm", ToastLength.Long).Show();
        }
    }
}