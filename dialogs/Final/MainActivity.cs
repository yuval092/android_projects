﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android;
using System;

namespace final
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        // EditText first, last, city;
        Button dateBtn;
        // Button genderBtn, saveBtn;
        DateTime date;
        DatePickerDialog datepick;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            //this.first = (EditText)FindViewById(Resource.Id.firstName);
            //this.last = (EditText)FindViewById(Resource.Id.lastName);
            //this.city = (EditText)FindViewById(Resource.Id.city);
            this.dateBtn = (Button)FindViewById(Resource.Id.dateBtn);
            //this.genderBtn = (Button)FindViewById(Resource.Id.genderBtn);
            //this.saveBtn = (Button)FindViewById(Resource.Id.saveBtn);

            this.dateBtn.Click += DateBtn_Click;
        }

        private void DateBtn_Click(object sender, System.EventArgs e)
        {
            this.date = DateTime.Today;
            this.datepick = new DatePickerDialog(this, onDatePick, date.Year, date.Month - 1, date.Day);

            datepick.Show();
        }

        private void onDatePick(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            string str = e.Date.ToLongDateString();
            Toast.MakeText(this, str, ToastLength.Long).Show();
            this.dateBtn.Text = str;
        }
    }
}