﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android;
using System;

namespace Date_Dialog
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btnDate, btnTime;
        DateTime date, time;
        DatePickerDialog datepick;
        TimePickerDialog timePick;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btnDate = (Button)FindViewById(Resource.Id.dateBtn);
            this.btnTime = (Button)FindViewById(Resource.Id.timeBtn);

            this.btnDate.Click += BtnDate_Click;
            this.btnTime.Click += BtnTime_Click;
        }

        private void BtnTime_Click(object sender, EventArgs e)
        {
            this.time = DateTime.Today;
            this.timePick = new TimePickerDialog(this, OnTimePick, this.time.Hour, this.time.Minute, true);
            this.timePick.Show();
        }

        private void BtnDate_Click(object sender, System.EventArgs e)
        {
            this.date = DateTime.Today;
            this.datepick = new DatePickerDialog(this, onDatePick, date.Year, date.Month - 1, date.Day);

            datepick.Show();
        }

        private void OnTimePick(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            string myText = e.HourOfDay + ":" + e.Minute;
            Toast.MakeText(this, myText, ToastLength.Long).Show();
            btnTime.Text = myText;
        }

        private void onDatePick(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            string str = e.Date.ToLongDateString();
            Toast.MakeText(this, str, ToastLength.Long).Show();
            this.btnDate.Text = str;
        }
    }
}