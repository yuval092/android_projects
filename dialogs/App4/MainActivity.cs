﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace App4
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Dialog dialog;
        Button btn1, btn2;
        EditText user, pass;
        ISharedPreferences sp;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btn1 = (Button)FindViewById(Resource.Id.loginBtn);
            this.sp = this.GetSharedPreferences("details", FileCreationMode.Private);

            this.btn1.Click += Btn1_Click;
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            CreateLoginDialog();
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this, "Welcome: " + user.Text, ToastLength.Long).Show();

            ISharedPreferencesEditor editor = this.sp.Edit();
            editor.PutString("username", user.Text);

            this.dialog.Dismiss();
        }

        public void CreateLoginDialog()
        {
            this.dialog = new Dialog(this);
            this.dialog.SetContentView(Resource.Layout.custom_layout);
            this.dialog.SetTitle("Connecting");
            this.dialog.SetCancelable(true);

            this.btn2 = (Button)this.dialog.FindViewById(Resource.Id.btn);
            this.user = (EditText)this.dialog.FindViewById(Resource.Id.username);
            this.pass = (EditText)this.dialog.FindViewById(Resource.Id.password);

            this.btn2.Click += Btn2_Click;

            this.dialog.Show();
        }
    }
}