﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;

namespace Ex2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        TextView tv;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.tv = (TextView)FindViewById(Resource.Id.tv);
            RegisterForContextMenu(this.tv);
        }

        public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
        {
            base.OnCreateContextMenu(menu, v, menuInfo);
            MenuInflater.Inflate(Resource.Menu.contextMenu, menu);
        }

        public override bool OnContextItemSelected(IMenuItem item)
        {
            base.OnContextItemSelected(item);
            if (item.ItemId == Resource.Id.first)
            {
                Toast.MakeText(this, "first was clicked", ToastLength.Long).Show();
                return true;
            }
            else if (item.ItemId == Resource.Id.second)
            {
                Toast.MakeText(this, "second was clicked", ToastLength.Long).Show();
                return true;
            }

            return false;
        }
    }
}