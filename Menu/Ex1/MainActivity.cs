﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Dialog dialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.optionsMenu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            base.OnOptionsItemSelected(item);

            if(item.ItemId == Resource.Id.loginItem)
            {
                CreateLoginDialog();
            }
            else if(item.ItemId == Resource.Id.registerItem)
            {
                CreateRegisterDialog();
            }
            else if(item.ItemId == Resource.Id.extraItem)
            {
                Toast.MakeText(this, "Extra was clicked", ToastLength.Long).Show();
            }

            return true;
        }

        public void CreateLoginDialog()
        {
            this.dialog = new Dialog(this);
            this.dialog.SetContentView(Resource.Layout.loginDialog);
            this.dialog.SetTitle("Login");
            this.dialog.SetCancelable(true);

            this.dialog.Show();
        }

        public void CreateRegisterDialog()
        {
            this.dialog = new Dialog(this);
            this.dialog.SetContentView(Resource.Layout.registerDialog);
            this.dialog.SetTitle("Login");
            this.dialog.SetCancelable(true);

            this.dialog.Show();
        }
    }
}