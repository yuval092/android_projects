﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using Android.Graphics;


namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView iv;
        Button btn;
        Bitmap bitmap;
        Button test;
        TextView scoreText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.iv = (ImageView)FindViewById(Resource.Id.iv);
            this.btn = (Button)FindViewById(Resource.Id.btnTakePic);
            this.test = (Button)FindViewById(Resource.Id.btnTest);
            this.scoreText = (TextView)FindViewById(Resource.Id.scoreText);

            this.btn.Click += Btn_Click;
            this.test.Click += Test_Click;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 0)
            {
                if (resultCode == Result.Ok)
                {
                    this.bitmap = (Android.Graphics.Bitmap)data.Extras.Get("data");
                    this.iv.SetImageBitmap(bitmap);
                }
            }
            if(requestCode == 1)
            {
                if(resultCode == Result.Ok)
                {
                    this.scoreText.Text = "Score: " + data.Extras.Get("score");
                }
                else if(resultCode == Result.Canceled)
                {
                    Toast.MakeText(this, "The test was canceled by user", ToastLength.Long).Show();
                }
            }
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(Android.Provider.MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 0);
        }

        private void Test_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(TestActivity));
            StartActivityForResult(intent, 1);
        }
    }
}