﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    [Activity(Label = "TestActivity")]
    public class TestActivity : Activity
    {
        RadioButton q1;
        RadioButton q2;
        Button btnFinishTest;
        Button btnCancelTest;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main2);

            this.q1 = (RadioButton)FindViewById(Resource.Id.q1s3);
            this.q2 = (RadioButton)FindViewById(Resource.Id.q2s2);
            this.btnFinishTest = (Button)FindViewById(Resource.Id.btnFinishTest);
            this.btnCancelTest = (Button)FindViewById(Resource.Id.btnCancelTest);

            this.btnFinishTest.Click += BtnFinishTest_Click;
            this.btnCancelTest.Click += BtnCancelTest_Click;
        }

        private void BtnFinishTest_Click(object sender, EventArgs e)
        {
            int score = 0;

            if (this.q1.Checked)
                score += 50;
            if (this.q2.Checked)
                score += 50;

            Intent intent2 = new Intent();
            intent2.PutExtra("score", score);
            SetResult(Result.Ok, intent2);
            Finish();
        }

        private void BtnCancelTest_Click(object sender, EventArgs e)
        {
            Intent intent2 = new Intent();
            SetResult(Result.Canceled, intent2);
            Finish();
        }
    }
}