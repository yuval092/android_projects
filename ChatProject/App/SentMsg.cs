﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    class SentMsg : Java.Lang.Object
    {
        private string msg;
        private bool isUserData;    // true - user data, false - program data

        public SentMsg(string msg, bool isUserData)
        {
            this.msg = msg;
            this.isUserData = isUserData;
        }

        public string Msg { get => msg; set => msg = value; }
        public bool IsUserData { get => isUserData; set => isUserData = value; }
    }
}