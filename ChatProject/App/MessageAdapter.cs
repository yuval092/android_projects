﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using App;

namespace App
{
    class MessageAdapter : BaseAdapter
    {
        Context context;
        List<SentMsg> data;
        LayoutInflater inflater;

        public MessageAdapter(Context context, List<SentMsg> data)
        {
            this.context = context;
            this.data = data;
            this.inflater = (LayoutInflater)this.context.GetSystemService(Context.LayoutInflaterService);
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return data[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count => data.Count;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View vi = null;
            if (data[position].IsUserData)
            {
                vi = inflater.Inflate(Resource.Layout.MyMessage, null);
            }
            else
            {
                vi = inflater.Inflate(Resource.Layout.HisMessage, null);
            }

            TextView messageView = vi.FindViewById<TextView>(Resource.Id.tvMsg);
            messageView.Text = data[position].Msg;

            return vi;
        }
    }
}