﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Graphics;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Android.Speech;
using Android.Content;
using System;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ListView lv;
        EditText txt;
        List<SentMsg> l;
        TcpClient client;
        ImageButton send, mic;
        MessageAdapter adapter;
        private readonly int VOICE = 10;

        const string HOST = "192.168.15.153";
        const int PORT = 9999;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            this.client = new TcpClient();
            // tryToConnect();

            this.lv = (ListView)FindViewById(Resource.Id.messages_view);
            this.send = (ImageButton)FindViewById(Resource.Id.send);
            this.mic = (ImageButton)FindViewById(Resource.Id.microphone);
            this.txt = (EditText)FindViewById(Resource.Id.txt);

            this.l = new List<SentMsg>();
            this.adapter = new MessageAdapter(this, this.l);
            this.lv.Adapter = this.adapter;

            this.send.Click += Send_Click;
            this.mic.Click += Mic_Click;
        }

        private void Mic_Click(object sender, System.EventArgs e)
        {
            string rec = Android.Content.PM.PackageManager.FeatureMicrophone;
            if (rec != "android.hardware.microphone")
            {
                // no microphone. Disable the button and output a toast.
                this.mic.Enabled = false;
                Toast.MakeText(this, "No microphone", ToastLength.Long).Show();
                return;
            }
            
            StartActivityForResult(defineVoiceIntent(), VOICE);
        }

        protected override void OnActivityResult(int requestCode, Result resultVal, Intent data)
        {
            if (requestCode == VOICE)
            {
                if (resultVal == Result.Ok)
                {
                    var matches = data.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
                    if (matches.Count != 0)
                    {
                        this.txt.Text = matches[0];
                    }
                    else
                    {
                        Toast.MakeText(this, "Speech failed", ToastLength.Short).Show();
                    }
                }
            }

            base.OnActivityResult(requestCode, resultVal, data);
        }

        private void Send_Click(object sender, System.EventArgs e)
        {
            if (this.txt.Text.Length > 0)
            {
                this.l.Add(new SentMsg(this.txt.Text, true));
                this.adapter.NotifyDataSetChanged();
                sendMessageToServer(this.txt.Text);
                this.txt.Text = "";
                this.lv.SetSelection(this.lv.Count - 1);
            }
        }

        private void tryToConnect()
        {
            this.client.Connect(HOST, PORT);
            Toast.MakeText(this, "Successfully connected to server", ToastLength.Long).Show();
        }

        private void sendMessageToServer(string msg)
        {
            //Stream stm = this.client.GetStream();

            //byte[] ba = new ASCIIEncoding().GetBytes(msg);
            //stm.Write(ba, 0, ba.Length);

            //byte[] bb = new byte[1000];
            //int k = stm.Read(bb, 0, 1000);

            this.l.Add(new SentMsg(this.txt.Text, false));
        }

        private Intent defineVoiceIntent()
        {
            // create the intent and start the activity
            Intent voiceIntent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);

            // if there is more then 1.5s of silence, consider the speech over
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 1500);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 1500);
            voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 15000);
            voiceIntent.PutExtra(RecognizerIntent.ExtraMaxResults, 1);
            voiceIntent.PutExtra(RecognizerIntent.ExtraLanguage, Java.Util.Locale.English);

            return voiceIntent;
        }
    }
}