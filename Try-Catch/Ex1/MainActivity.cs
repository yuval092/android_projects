﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText et1, et2;
        Button btn1, btn2;
        TextView tv;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.et1 = (EditText)FindViewById(Resource.Id.et1);
            this.et2 = (EditText)FindViewById(Resource.Id.et2);
            this.btn1 = (Button)FindViewById(Resource.Id.btn1);
            this.btn2 = (Button)FindViewById(Resource.Id.btn2);
            this.tv = (TextView)FindViewById(Resource.Id.tv);

            this.btn1.Click += Btn1_Click;
            this.btn2.Click += Btn2_Click;
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            try
            {
                int num1 = int.Parse(this.et1.Text);
                int num2 = int.Parse(this.et2.Text);
                int divided = num1 / num2;
                double sqrt = func(num1);

                this.tv.Text = divided.ToString() + " " + sqrt.ToString();
            }
            catch(FormatException f)
            {
                Toast.MakeText(this, "Not a number", ToastLength.Long).Show();
                this.tv.Text = "Not a number";
            }
            catch(DivideByZeroException d)
            {
                Toast.MakeText(this, "Can't divide by zero", ToastLength.Long).Show();
                this.tv.Text = "can't divide by zero";
            }
            catch(Exception ex)
            {
                Toast.MakeText(this, "Error: " + ex.Message, ToastLength.Long).Show();
            }
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            try
            {
                int num = int.Parse(this.et1.Text);
                this.tv.Text = num.ToString().Length.ToString();
            }
            catch
            {
                Toast.MakeText(this, "Not a number", ToastLength.Long).Show();
                this.tv.Text = "Not a number";
            }
            finally
            {
                Toast.MakeText(this, "Thanks", ToastLength.Long).Show();
            }
        }

        private double func(int num)
        {
            if (num < 0)
                throw new Exception("Negative number");
            return Math.Sqrt(num);
        }
    }
}