﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using Android.Provider;
using App.Database;
using System;

namespace App
{
    [Activity(Label = "@string/app_name", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button loginBtn;
        EditText user, password;
        TextView registerLabel;
        BroadcastBattery battery;
        bool switchPage;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.loginBtn = (Button)FindViewById(Resource.Id.login);
            this.user = (EditText)FindViewById(Resource.Id.username);
            this.password = (EditText)FindViewById(Resource.Id.password);
            this.registerLabel = (TextView)FindViewById(Resource.Id.register);

            this.switchPage = false;
            this.battery = new BroadcastBattery();
            RegisterReceiver(battery, new IntentFilter(Intent.ActionBatteryChanged));

            this.loginBtn.Click += LoginBtn_Click;
            this.registerLabel.Click += Register_Click;
        }


        private void Register_Click(object sender, System.EventArgs e)
        {
            switchPage = true;
            Intent intent = new Intent(this, typeof(Signup));
            StartActivityForResult(intent, 0);
        }


        private void LoginBtn_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Menu));
            if (this.user.Text != "" && this.password.Text != "")
            {
                if (DatabaseManager.login(this.user.Text, this.password.Text))
                {
                    switchPage = true;
                    StartActivityForResult(intent, 0);
                }
                else
                {
                    Toast.MakeText(this, "Username of password is invalid", ToastLength.Long).Show();
                }
            }
            else
            {
                Toast.MakeText(this, "Fill username text field", ToastLength.Long).Show();
            }
        }


        // for when the pages come back
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switchPage = false;
        }


        protected override void OnStop()
        {
            base.OnStop();
            if (!switchPage)
            {
                Intent intent = new Intent(this, typeof(MyService));
                StartService(intent);
            }
        }
    }
}