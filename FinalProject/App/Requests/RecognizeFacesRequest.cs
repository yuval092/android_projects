﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App.Requests
{
    class RecognizeFacesRequest : Request
    {
        [JsonPropertyAttribute]
        public string img_data { get; set; }

        public RecognizeFacesRequest(string image) : base(1)
        {
            this.img_data = image;
        }
        ~RecognizeFacesRequest() { }
    }
}