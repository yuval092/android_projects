﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App.Requests
{
    class Request
    {
        [JsonProperty]
        public int request_code { get; set; }

        public Request(int code)
        {
            this.request_code = code;
        }
        ~Request() { }
    }
}