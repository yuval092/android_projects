﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Classes;

namespace App.Requests
{
    class AddStudentsRequest : Request
    {
        [JsonProperty]
        public Dictionary<string, List<string>> students_images { get; set; }     // { name: [img1_base64, img2_base64] }

        public AddStudentsRequest(Dictionary<string, List<string>> students_images) : base(0)
        {
            this.students_images = students_images;
        }
        ~AddStudentsRequest() { }
    }
}