﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Database;
using Java.IO;
using Newtonsoft.Json;
using App.Requests;
using Android.Support.V4.Content;
using App.Responses;
using App.Classes;
using System.Threading;

namespace App
{
    [Activity(Label = "Menu")]
    public class Menu : Activity
    {
        Button cameraBtn, managerBtn, historyBtn, menuBtn, updateBtn;
        Dialog profileDialog, progressDialog;
        EditText newPasswordInput;
        private string filePath;
        bool switchPage;

        // calls on the creation of the activity
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.menu);

            this.cameraBtn = (Button)FindViewById(Resource.Id.camera);
            this.managerBtn = (Button)FindViewById(Resource.Id.manager);
            this.historyBtn = (Button)FindViewById(Resource.Id.files);
            this.menuBtn = (Button)FindViewById(Resource.Id.menu);
            this.filePath = "";
            this.switchPage = false;

            RegisterForContextMenu(menuBtn);

            this.cameraBtn.Click += Camera_Click;
            this.managerBtn.Click += Manager_Click;
            this.historyBtn.Click += Files_Click;
        }


        // called when we exit the activity
        protected override void OnStop()
        {
            base.OnStop();
            if (!switchPage)
            {
                Intent intent = new Intent(this, typeof(MyService));
                StartService(intent);
            }
        }


        // creates the menu
        public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
        {
            base.OnCreateContextMenu(menu, v, menuInfo);
            MenuInflater.Inflate(Resource.Menu.optionsMenu, menu);
        }


        // handles the selection of items from the menu
        public override bool OnContextItemSelected(IMenuItem item)
        {
            base.OnOptionsItemSelected(item);
            switchPage = true;

            if(item.ItemId == Resource.Id.encodeItem)
            {
                this.progressDialog = ProgressDialog.Show(this, "", "Waiting for server response...", true);
                new Thread(() => HandleEncodeRequest()).Start();

            }
            else if (item.ItemId == Resource.Id.profileItem)
            {
                CreateProfileDialog();
            }
            else if (item.ItemId == Resource.Id.logoutItem)
            {
                Finish();
            }

            return true;
        }


        // creates the profile dialog
        public void CreateProfileDialog()
        {
            this.profileDialog = new Dialog(this);
            this.profileDialog.SetContentView(Resource.Layout.Profile);
            this.profileDialog.SetTitle("Profile Settings");
            this.profileDialog.SetCancelable(true);

            this.newPasswordInput = (EditText)this.profileDialog.FindViewById(Resource.Id.newPassword);
            this.updateBtn = (Button)this.profileDialog.FindViewById(Resource.Id.updateData);

            updateBtn.Click += UpdateBtn_Click;

            this.profileDialog.Show();
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            if (this.newPasswordInput.Text != "")
            {
                DatabaseManager.updatePassword(this.newPasswordInput.Text);
                Toast.MakeText(this, "Updated successfully", ToastLength.Long).Show();
                this.profileDialog.Dismiss();
                switchPage = false;
            }
            else
            {
                Toast.MakeText(this, "Please fill the input field", ToastLength.Long).Show();
            }
        }


        // when the camera is clicked
        private void Camera_Click(object sender, EventArgs e)
        {
            try
            {
                Intent intent = new Intent(MediaStore.ActionImageCapture);

                Java.IO.File storageDir = GetExternalFilesDir(Android.OS.Environment.DirectoryPictures);
                Java.IO.File exportFile = Java.IO.File.CreateTempFile("image", ".png", storageDir);
                Android.Net.Uri exportUri = Android.Support.V4.Content.FileProvider.GetUriForFile(this, "com.company.app.fileprovider", exportFile);
                this.filePath = exportFile.AbsolutePath;

                switchPage = true;
                intent.PutExtra(MediaStore.ExtraOutput, exportUri);
                StartActivityForResult(intent, 0);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
            }
        }


        // when the manager button is clicked
        private void Manager_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ManageClasses));
            switchPage = true;
            StartActivityForResult(intent, 1);
        }


        // when the files button is clicked
        private void Files_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(History));
            switchPage = true;
            StartActivity(intent);
        }


        // when a new request has been made (recognize faces in new image or add new students)
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switchPage = false;

            try
            { 
                if (requestCode == 0 && resultCode == Result.Ok)        // recognize faces
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(this.filePath);
                    Bitmap bp = Helper.getResizedBitmap(bytes, 600);

                    MemoryStream stream = new MemoryStream();
                    bp.Compress(Bitmap.CompressFormat.Png, 85, stream);

                    string base64Image = System.Convert.ToBase64String(stream.ToArray());
                    RecognizeFacesRequest req = new RecognizeFacesRequest(base64Image);

                    this.progressDialog = ProgressDialog.Show(this, "", "Waiting for server response...", true);
                    new Thread(() => HandleRecognizeRequest(JsonConvert.SerializeObject(req))).Start();



                    /*
                        The server returns the list of found students. we need to present it to the user,
                        and add their attendence to the database. The database will consist of:
                        1. classes table (id, class grade, class number).
                        2. students table (id, full name, class id (foreign key from classes table).
                        3. images table (id, student id (foreign key from studens table), image data in base64).
                        4. attendance table (id, student id, date, lesson number, attended(true/false)).

                        in this function we update the attendance table.
                    */
                }
                else if (requestCode == 1 && resultCode == Result.Ok)    // add students
                {
                    Dictionary<string, List<string>> studentsData = App.Classes.Bridge.getInstance().studentsImages;
                    if (studentsData != null)
                    {
                        this.progressDialog = ProgressDialog.Show(this, "", "Waiting for server response...", true);
                        new Thread(() => HandleAddRequest(new AddStudentsRequest(studentsData))).Start();
                    }
                        
                    App.Classes.Bridge.getInstance().studentsImages = null;   // reset


                    /*
                        All the server has to do with these requests is organize the folders and add images.
                        * In the add classes request, the server will create the requested folder.
                        * In the add students request, the server will create the student's folders inside the
                        correct class (if class doesn't exist - error), and add the transmitted pictures to it.

                        On the other hand, the client has some stuff to do with these requests.
                        On the add classes request, the client must insert new class to the database.
                        On the add students request, the client must insert new student and his pictures in base64 to the database.

                        In this function we update the classes table and the students table and the images table.
                    */
                }
            }
            catch (Exception e)
            {
                Toast.MakeText(this, e.Message, ToastLength.Long).Show();
            }
        }


        // handles the add new students request
        private void HandleAddRequest(AddStudentsRequest request)
        {
            string req = JsonConvert.SerializeObject(request);
            bool result = Helper.SendAddStudentRequest(req);

            this.progressDialog.Dismiss();

            if (!result)   // failed
            {
                Toast.MakeText(this, "Failed to add student", ToastLength.Long).Show();
                return;
            }

            // now add to the database.
            foreach (var element in request.students_images)
            {
                DatabaseManager.insertNewStudent(element.Key);
                DatabaseManager.insertNewImages(element.Key, element.Value);
            }
        }


        // handles the recognize new image request
        private void HandleRecognizeRequest(string req)
        {
            RecognizeFacesResponse resp = Helper.SendRecognizeRequest(req);
            this.progressDialog.Dismiss();
            if (resp == null)
            {
                Toast.MakeText(this, "Failed to communicate with the server.", ToastLength.Long).Show();
                return;
            }

            switchPage = true;
            Intent i = new Intent(this, typeof(MarkedImage));
            byte[] bytes = Convert.FromBase64String(resp.marked_image);
            Bridge.getInstance().markedImage = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
            StartActivity(i);

            // add attendences
            List<Student> students = DatabaseManager.selectAllStudents();
            List<Attendance> attendances = DatabaseManager.selectAllAttendances();
            foreach (var student in students)
            {
                if (resp.names.Contains<string>(student.Name))
                    DatabaseManager.insertNewAttendence(student.Id, true);
                else
                    DatabaseManager.insertNewAttendence(student.Id, false);
            }
        }


        // handles the encode new student's images request
        private void HandleEncodeRequest()
        {
            bool result = Helper.SendEncodeRequest();

            this.progressDialog.Dismiss();
            switchPage = false;

            if (!result)   // failed
                Toast.MakeText(this, "Failed to encode images, new students won't be recognized", ToastLength.Long).Show();
        }
    }
}