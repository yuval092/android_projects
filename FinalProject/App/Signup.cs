﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Database;

namespace App
{
    [Activity(Label = "Signup")]
    public class Signup : Activity
    {
        Button signup;
        EditText firstName, lastName, username, email, password;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.signup);

            this.signup = (Button)FindViewById(Resource.Id.signup);
            this.firstName = (EditText)FindViewById(Resource.Id.firstName);
            this.lastName = (EditText)FindViewById(Resource.Id.lastName);
            this.username = (EditText)FindViewById(Resource.Id.username);
            this.email = (EditText)FindViewById(Resource.Id.email);
            this.password = (EditText)FindViewById(Resource.Id.password);

            this.signup.Click += Signup_Click;
        }

        private void Signup_Click(object sender, EventArgs e)
        {
            // add checks
            string first = this.firstName.Text;
            string last = this.lastName.Text;
            string email = this.email.Text;
            string username = this.username.Text;
            string pass = this.password.Text;
            if (first == "" || last == "" || email == "" || username == "" || pass == "")
                return;
            
            // signup to database
            User user = new User(first, last, email, username, pass);
            if (!DatabaseManager.insertNewUser(user))
                Toast.MakeText(this, "Username already exists", ToastLength.Long).Show();
            Finish();
        }
    }
}