﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using App.Requests;
using Newtonsoft.Json;
using System.IO;
using Android.Provider;
using App.Classes;
using Android.Support.V4.Content;
//using Android.Hardware.Camera2;

namespace App
{
    [Activity(Label = "ManageClasses")]
    public class ManageClasses : Activity
    {
        EditText name;
        Button addStudentBtn, pickImageBtn, takeImageBtn, finishActivityBtn;
        ListView imagesList;

        // other
        Dictionary<string, List<string>> studentsData;
        MessageAdapter adapter;
        List<String> images;
        List<string> items;
        string path;
        int count;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ManageClasses);

            this.name = (EditText)FindViewById(Resource.Id.fullName);
            this.addStudentBtn = (Button)FindViewById(Resource.Id.add);
            this.pickImageBtn = (Button)FindViewById(Resource.Id.pickBtn);
            this.takeImageBtn = (Button)FindViewById(Resource.Id.takeBtn);
            this.finishActivityBtn = (Button)FindViewById(Resource.Id.finish);
            this.imagesList = (ListView)FindViewById(Resource.Id.imagesList);

            this.items = new List<string>();
            this.adapter = new MessageAdapter(this, this.items);
            this.imagesList.Adapter = this.adapter;
            this.studentsData = new Dictionary<string, List<string>>();
            this.images = new List<string>();
            this.path = null;
            this.count = 0;

            this.addStudentBtn.Click += AddStudent_Click;
            this.takeImageBtn.Click += TakeBtn_Click;
            this.pickImageBtn.Click += PickBtn_Click;
            this.finishActivityBtn.Click += FinishBtn_Click;
        }


        // when the add student button is clicked
        private void AddStudent_Click(object sender, EventArgs e)
        {
            if (this.images.Count == 0)
                Toast.MakeText(this, "You must add images of the new student !", ToastLength.Long).Show();
            else if (this.name.Text == "")
                Toast.MakeText(this, "You must insert the name of the new student !", ToastLength.Long).Show();
            else if (!this.studentsData.ContainsKey(this.name.Text))
                this.studentsData.Add(this.name.Text, new List<string>(this.images));

            this.images.Clear();    // gotta delete pictures after every insertion
            Toast.MakeText(this, "Added student", ToastLength.Long).Show();
        }


        // when the finish actiivty button is clicked
        private void FinishBtn_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent();
            Bridge.getInstance().studentsImages = this.studentsData;
            SetResult(Result.Ok, intent);
            Finish();
        }


        // when the take image button is clicked
        private void TakeBtn_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);

            // required for the putExtra
            Java.IO.File storageDir = GetExternalFilesDir(Android.OS.Environment.DirectoryPictures);
            Java.IO.File exportFile = Java.IO.File.CreateTempFile("image", ".png", storageDir);
            Android.Net.Uri exportUri = Android.Support.V4.Content.FileProvider.GetUriForFile(this, "com.company.app.fileprovider", exportFile);
            this.path = exportFile.AbsolutePath;

            intent.PutExtra(MediaStore.ExtraOutput, exportUri);     // captures the image at full screen size
            StartActivityForResult(intent, 0);
        }


        // when the pick image button is clicked
        private void PickBtn_Click(object sender, EventArgs e)
        {
            getImageFromFileSystem();
        }


        // handles the list view representing chosen images
        private void handleListView(string fileName)
        {
            this.items.Add(fileName);
            this.adapter.NotifyDataSetChanged();
            this.imagesList.SetSelection(this.imagesList.Count - 1);
        }


        // gets image from the file system using the CrosFilePicker
        private async void getImageFromFileSystem()
        {
            try
            {
                FileData fileData = await CrossFilePicker.Current.PickFile();

                if (fileData == null)
                    return; // user canceled file picking
                
                handlePicturesList(fileData.DataArray);     // add to list

                handleListView(fileData.FileName);          // add t-o list view
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                Toast.MakeText(this, ex.ToString(), ToastLength.Long).Show();
            }
        }


        // when the activity is called back after calling a intent
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 0 && resultCode == Result.Ok)        // take picture 
            {
                byte[] bytes = System.IO.File.ReadAllBytes(this.path);
                Bitmap bp = Helper.getResizedBitmap(bytes, 600);

                MemoryStream stream = new MemoryStream();
                bp.Compress(Bitmap.CompressFormat.Jpeg, 85, stream);

                handlePicturesList(stream.ToArray());

                handleListView("from_camera_" + this.count.ToString());
                this.count++;
            }
        }


        // handles the pictures list to be added to the json in return to menu
        private void handlePicturesList(byte[] imgBytes)
        {
            Bitmap bp = Helper.getResizedBitmap(imgBytes, 600);

            MemoryStream stream = new MemoryStream();
            bp.Compress(Bitmap.CompressFormat.Jpeg, 85, stream);
            string str = Convert.ToBase64String(imgBytes);
            string name = this.name.Text;

            if (!this.images.Contains(str))
            {
                if (this.studentsData.ContainsKey(name) && this.studentsData[name].Contains(str))
                    throw new Exception("Image already added");
                else
                    this.images.Add(str);
            }
        }
    }
}