﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace App.Database
{
    class DatabaseManager
    {
        private static SQLiteConnection conn;
        private static int currentUserId;

        // static constructor initalizing the database
        static DatabaseManager()
        {
            string path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "dbFile");
            conn = new SQLiteConnection(path);
            currentUserId = -1;

            // creates the tables if they don't exist
            conn.CreateTable<User>();
            conn.CreateTable<Student>();
            conn.CreateTable<Image>();
            conn.CreateTable<Attendance>();

        }


        // gets the user id from the users table
        public static int getUserId(string username)
        {
            List<User> user = conn.Query<User>("SELECT * FROM Users WHERE Username = ?", username);
            return user.Count == 1 ? user[0].Id : -1;
        }


        // inserts new user to the users table
        public static bool insertNewUser(User user)
        {
            if (getUserId(user.Username) != -1)
                return false;

            conn.Insert(user);
            return true;
        }
        

        // checks if a login is possible (if there's a username and the password match)
        public static bool login(string username, string password)
        {
            List<User> user = conn.Query<User>("SELECT Id FROM Users WHERE Username = ? AND Password = ?", username, password);
            if (user.Count == 1)
            {
                currentUserId = user[0].Id;
                return true;
            }

            return false;
        }


        // gets the id of a student from the students table
        public static int getStudentId(string studentName)
        {
            List<Student> d = conn.Query<Student>("SELECT * FROM Students WHERE Name = ?", studentName);
            return d.Count == 1 ? d[0].Id : -1;
        }


        // get student name by its id
        public static string getStudentName(int id)
        {
            List<Student> d = conn.Query<Student>("SELECT * FROM Students WHERE Id = ?", id);
            return d.Count == 1 ? d[0].Name : null;
        }


        // inserts a new student to the students table
        public static bool insertNewStudent(string name)
        {
            if (getStudentId(name) != -1)       // Student already exists
                return false;

            conn.Insert(new Student(name, currentUserId));
            return true;
        }

        // gets the attendances of a student by its name and date of attendance
        public static List<string> getStudentAttendances(string studentName, string date = "default")     // date is an optional parameter
        {
            // if data != "default" { find attendances in specified date (validate date first) }

            // leave date for now, maybe a feature
            int id = getStudentId(studentName);
            if (id == -1)
                return null;
            
            List<Attendance> data;
            if(date == "default")
                data = conn.Query<Attendance>("SELECT * FROM Attendances WHERE studentId = ?", id);
            else
                data = conn.Query<Attendance>("SELECT * FROM Attendances WHERE studentId = ? AND Date = ?", id, date);

            List<string> finalData = new List<string>();
            foreach (var a in data)
            {
                finalData.Add(string.Format("Name: {0}, Date: {1}, Attended: {2}", 
                    getStudentName(a.StudentId), a.Date, a.HasAttended == 1 ? "Yes" : "No"));
            }

            return finalData;
        }


        // gets the attendances in a specific date
        public static List<string> getDateAttendances(string date)     // date is an optional parameter
        {
            List<Attendance> data = conn.Query<Attendance>("SELECT * FROM Attendances WHERE date = ?", date);
            List<string> finalData = new List<string>();
            foreach (var a in data)
            {
                finalData.Add(string.Format("Name: {0}, Date: {1}, Attended: {2}",
                    getStudentName(a.StudentId), a.Date, a.HasAttended == 1 ? "Yes" : "No"));
            }

            return finalData;
        }


        // inserts a new image to the images table
        public static bool insertNewImages(string name, List<string> images)
        {
            int id = getStudentId(name);
            if (id == -1)
                return false;

            foreach (var img in images)
            {
                conn.Insert(new Image(id, img));
            }

            return true;
        }


        // inserts a new attendance to the attendences table
        public static void insertNewAttendence(int id, bool hasAttended)
        {
            string date = DateTime.Now.ToString("d/M/yyyy");
            conn.Insert(new Attendance(id, date, hasAttended == true ? 1 : 0));
        }


        // gets all the students in the students table
        public static List<Student> selectAllStudents()
        {
            return conn.Query<Student>("SELECT * From Students");
        }

        // gets all the attendances in the students table
        public static List<Attendance> selectAllAttendances()
        {
            return conn.Query<Attendance>("SELECT * From Attendances");
        }


        // update the password for current user
        public static bool updatePassword(string pass)
        {
            int res = conn.Execute("UPDATE Users SET Password = ? WHERE Id = ?", pass, currentUserId);
            return res == 1;
        }
    }
}