﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLiteNetExtensions.Attributes;
using SQLite;

namespace App.Database
{
    [Table("Users")]
    class User
    {
        [PrimaryKey, AutoIncrement, Column("Id")]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public User() { }
        public User(string FirstName, string LastName, string Email, string Username, string Password)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Username = Username;
            this.Password = Password;
        }
    }


    [Table("Students")]
    class Student
    {
        [PrimaryKey, AutoIncrement, Column("Id")]
        public int Id { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        [ForeignKey(typeof(User))]
        public int UserId { get; set; }

        public Student() { }
        public Student(string Name, int UserId)
        {
            this.Name = Name;
            this.UserId = UserId;
        }
    }


    [Table("Images")]
    class Image
    {
        [PrimaryKey, AutoIncrement, Column("Id")]
        public int Id { get; set; }
        [ForeignKey(typeof(Student))]
        public int StudentId { get; set; }
        public string ImageData { get; set; }

        public Image() { }
        public Image(int studentId, string imageData)
        {
            this.StudentId = studentId;
            this.ImageData = imageData;
        }
    }


    [Table("Attendances")]
    class Attendance
    {
        [PrimaryKey, AutoIncrement, Column("Id")]
        public int Id { get; set; }
        [ForeignKey(typeof(Student))]
        public int StudentId { get; set; }
        public string Date { get; set; }
        public int HasAttended { get; set; }        // 1 = true, 0 = false

        public Attendance() { }
        public Attendance(int studentId, string date, int hasAttended)
        {
            this.StudentId = studentId;
            this.Date = date;
            this.HasAttended = hasAttended;
        }
    }
}