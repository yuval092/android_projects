﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App.Responses
{
    class RecognizeFacesResponse : Response
    {
        public List<string> names { get; set; }     // names found on image

        public string marked_image { get; set; }         // the image with markers

        public RecognizeFacesResponse(List<string> names, string img) : base(0)
        {
            this.names = names;
            this.marked_image = img;
        }
        ~RecognizeFacesResponse() { }
    }
}