﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using App;

namespace App
{
    class MessageAdapter : BaseAdapter
    {
        Context context;
        List<string> data;
        LayoutInflater inflater;

        public MessageAdapter(Context context, List<string> data)
        {
            this.context = context;
            this.data = data;
            this.inflater = (LayoutInflater)this.context.GetSystemService(Context.LayoutInflaterService);
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return data[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count => data.Count;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View vi = null;
            vi = inflater.Inflate(Resource.Layout.listItem, null);

            TextView messageView = vi.FindViewById<TextView>(Resource.Id.tvMsg);
            messageView.Text = data[position];

            return vi;
        }
    }
}