﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


/*
 * An singelton class used for transferring data between intents.
 * I created this class because the putExtra fails with giant data.
 */

namespace App.Classes
{
    public class Bridge
    {
        private static Bridge instance = null;
        public Dictionary<string, List<string>> studentsImages;
        public Bitmap markedImage;

        public static Bridge getInstance()
        {
            if (instance == null)
            {
                instance = new Bridge();
            }

            return instance;
        }

        private Bridge()
        {
            this.studentsImages = null;
            this.markedImage = null;
        }
    }
}