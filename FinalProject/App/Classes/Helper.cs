﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using Newtonsoft.Json;
using App.Responses;
using System.IO;
using Android.Graphics;

namespace App
{
    class Helper
    {
        private static readonly string HOST = "http://ed3feb23.ngrok.io";
        private static readonly string RECOGNIZE_PATH = "/recognize/";
        private static readonly string ADD_STUDENT_PATH = "/add/";
        private static readonly string ENCODE_STUDENT_PATH = "/encode/";


        // send a recognize new image request to the server and handles the
        // result recieved from the server
        public static RecognizeFacesResponse SendRecognizeRequest(string req)
        {
            string address = HOST + RECOGNIZE_PATH;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(address);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = -1;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(req);
            }

            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                return JsonConvert.DeserializeObject<RecognizeFacesResponse>(result);
            }
            catch { return null; }
        }


        // send a add new students requests to the server and
        // handles the result recieved from the server
        public static bool SendAddStudentRequest(string req)
        {
            string address = HOST + ADD_STUDENT_PATH;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(address);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = -1;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(req);
            }

            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                return new StreamReader(httpResponse.GetResponseStream()).ReadToEnd() == "1";
            }
            catch { return false; }
        }


        // send a recognize new image request to the server and handles the
        // result recieved from the server
        public static bool SendEncodeRequest()
        {
            string address = HOST + ENCODE_STUDENT_PATH;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(address);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Timeout = -1;

            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                return new StreamReader(httpResponse.GetResponseStream()).ReadToEnd() == "1";
            }
            catch{ return false; }
        }


        // changes the size of a bitmap according to a given size in order to 
        // change the size of the image bytes transferred through the http request
        public static Bitmap getResizedBitmap(byte[] bytes, int maxSize)
        {
            Bitmap image = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
            int width = image.Width;
            int height = image.Height;

            float bitmapRatio = (float)width / (float)height;
            if (bitmapRatio > 1)
            {
                width = maxSize;
                height = (int)(width / bitmapRatio);
            }
            else
            {
                height = maxSize;
                width = (int)(height * bitmapRatio);
            }
            return Bitmap.CreateScaledBitmap(image, width, height, true);
        }
    }
}