﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{ 
    class MyHandler : Handler
    {
        private Context context;

        public MyHandler(Context context)
        {
            this.context = context;
        }

        public override void HandleMessage(Message msg)
        {
            if (msg.Arg1 == 0)
                Toast.MakeText(this.context, "See you next time!", ToastLength.Long).Show();
        }
    }
}