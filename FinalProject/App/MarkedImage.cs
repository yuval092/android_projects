﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Classes;

namespace App
{
    [Activity(Label = "MarkedImage")]
    public class MarkedImage : Activity
    {
        ImageView iv;
        Button btn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MarkedImage);

            this.iv = (ImageView)FindViewById(Resource.Id.marked_image_view);
            this.btn = (Button)FindViewById(Resource.Id.okBtn);

            Bitmap img = Bridge.getInstance().markedImage;
            Bridge.getInstance().markedImage = null;
            if (img != null)
                this.iv.SetImageBitmap(img);
            else
                Toast.MakeText(this, "Failed to present attendance image", ToastLength.Long).Show();

            this.btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            Finish();
        }
    }
}