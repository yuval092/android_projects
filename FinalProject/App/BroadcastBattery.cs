﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    class BroadcastBattery : BroadcastReceiver
    {
        bool alreadySent;

        public BroadcastBattery()
        {
            alreadySent = false;
        }

        // handles what to do with the broadcast reciever when there is new data
        public override void OnReceive(Context context, Intent intent)
        {
            int battery = intent.GetIntExtra("level", 0);
            if (battery < 15 && battery != 0 && !alreadySent)
            {
                Toast.MakeText(context, "Low battry, maybe call it a day?", ToastLength.Long).Show();
                alreadySent = true;
            }
        }
    }
}