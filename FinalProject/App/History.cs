﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Database;

namespace App
{
    [Activity(Label = "History")]
    public class History : Activity
    {
        EditText nameInput, dateInput;
        List<string> finalData;
        MessageAdapter adapter;
        Button showDataBtn;
        ListView dataList;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.History);

            this.nameInput = (EditText)FindViewById(Resource.Id.nameInput);
            this.dateInput = (EditText)FindViewById(Resource.Id.dateInput);
            this.showDataBtn = (Button)FindViewById(Resource.Id.historyBtn);
            this.dataList = (ListView)FindViewById(Resource.Id.dataList);

            this.finalData = new List<string>();
            this.adapter = new MessageAdapter(this, this.finalData);
            this.dataList.Adapter = this.adapter;

            this.showDataBtn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            List<string> result = null;

            // clear list view in every click
            this.finalData.Clear();
            this.adapter.NotifyDataSetChanged();

            if(this.nameInput.Text != "")
            {
                if(this.dateInput.Text != "")
                    result = DatabaseManager.getStudentAttendances(this.nameInput.Text, this.dateInput.Text);
                else
                    result = DatabaseManager.getStudentAttendances(this.nameInput.Text);
            }
            else if(this.dateInput.Text != "")
            {
                result = DatabaseManager.getDateAttendances(this.dateInput.Text);
            }
            else
            {
                Toast.MakeText(this, "You must fill the input fields!", ToastLength.Long).Show();
                return;
            }

            HandleListView(result);
        }

        private void HandleListView(List<string> data)
        {
            if(data == null)
            {
                Toast.MakeText(this, "Student not found", ToastLength.Long).Show();
                return;
            }

            if (data.Count == 0)
                this.finalData.Add("No results");
            else
                this.finalData.AddRange(data);

            this.adapter.NotifyDataSetChanged();
        }
    }
}