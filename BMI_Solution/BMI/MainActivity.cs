﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace BMI
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        TextView heightView;
        EditText heightEdit;
        TextView weightView;
        SeekBar  weightBar;
        TextView bmiView;
        ImageView thinPic;
        ImageView normalPic;
        ImageView fatPic;
        Toast mToast;
        Switch sw;

        bool switchChecked = true;
        int pic = -1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            heightView =    (TextView)FindViewById(Resource.Id.heightView);
            heightEdit =    (EditText)FindViewById(Resource.Id.heightEdit);
            weightView =    (TextView)FindViewById(Resource.Id.weightView);
            weightBar =     (SeekBar)FindViewById(Resource.Id.weightBar);
            bmiView =       (TextView)FindViewById(Resource.Id.bmiView);
            thinPic =       (ImageView)FindViewById(Resource.Id.thinPic);
            normalPic =     (ImageView)FindViewById(Resource.Id.normalPic);
            fatPic =        (ImageView)FindViewById(Resource.Id.fatPic);
            sw =            (Switch)FindViewById(Resource.Id.sw);

            sw.CheckedChange += Sw_CheckedChange;
            weightBar.ProgressChanged += weightBar_ProgressChanged;
        }

        private void Sw_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (e.IsChecked)            //switch is on.
            {
                switchChecked = true;

                if (pic == 0)
                {
                    thinPic.Visibility = Android.Views.ViewStates.Visible;
                }
                else if (pic == 1)
                {
                    normalPic.Visibility = Android.Views.ViewStates.Visible;
                }
                else if (pic == 2)
                {
                    fatPic.Visibility = Android.Views.ViewStates.Visible;
                }
            }
            else
            {
                switchChecked = false;
                if (mToast != null)
                {
                    mToast.Cancel();
                }


                thinPic.Visibility = Android.Views.ViewStates.Invisible;
                normalPic.Visibility = Android.Views.ViewStates.Invisible;
                fatPic.Visibility = Android.Views.ViewStates.Invisible;
            }
        }

        private void weightBar_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            if(switchChecked == false)      //switch is off.
                return;

            float weight = e.Progress;
            weightView.Text = "Enter your weight: " + weight;

            string height = heightEdit.Text;
            double heightInt = 1;

            if(height != null && height != "")
            {
                heightInt = double.Parse(height) / 100.0;
            }

            int ans = (int)(weight / (heightInt * heightInt));
            bmiView.Text = "Your BMI: " + ans;

            string msg = "";
            if(ans < 18)
            {
                thinPic.Visibility = Android.Views.ViewStates.Visible;
                normalPic.Visibility = Android.Views.ViewStates.Invisible;
                fatPic.Visibility = Android.Views.ViewStates.Invisible;
                msg = "אתה בתת משקל !";
                pic = 0;
            }
            else if(ans < 23)
            {
                thinPic.Visibility = Android.Views.ViewStates.Invisible;
                normalPic.Visibility = Android.Views.ViewStates.Visible;
                fatPic.Visibility = Android.Views.ViewStates.Invisible;
                msg = "אתה במשקל תקין !";
                pic = 1;
            }
            else
            {
                thinPic.Visibility = Android.Views.ViewStates.Invisible;
                normalPic.Visibility = Android.Views.ViewStates.Invisible;
                fatPic.Visibility = Android.Views.ViewStates.Visible;
                msg = "אתה במשקל עודף !";
                pic = 2;
            }

            showAToast(msg);
        }

        public void showAToast(string message)
        {
            if (mToast != null)
            {
                mToast.Cancel();
            }
            mToast = Toast.MakeText(this, message, ToastLength.Long);
            mToast.Show();
        }
    }
}