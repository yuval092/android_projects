﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.IO;
using System.Text;

namespace Ex
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btn1, btn2, btn3, btn4;
        EditText et;
        TextView tv1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btn1 = (Button)FindViewById(Resource.Id.btn1);
            this.btn2 = (Button)FindViewById(Resource.Id.btn2);
            this.btn3 = (Button)FindViewById(Resource.Id.btn3);
            this.btn4 = (Button)FindViewById(Resource.Id.btn4);
            this.et = (EditText)FindViewById(Resource.Id.et);
            this.tv1 = (TextView)FindViewById(Resource.Id.txt);

            this.btn1.Click += Btn1_Click;
            this.btn2.Click += Btn2_Click;
            this.btn3.Click += Btn3_Click;
            this.btn4.Click += Btn4_Click;
        }

        private void Btn4_Click(object sender, System.EventArgs e)
        {
            readFromFile("emailsTemp.txt");
        }

        private void Btn3_Click(object sender, System.EventArgs e)
        {
            writeToTemp(this.et.Text);
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            readFromFile("emails.txt");
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            if (!this.et.Text.Contains('@'))
            {
                Toast.MakeText(this, "Invalid email", ToastLength.Long).Show();
                return;
            }

            appendToFile(this.et.Text);
        }


        private void appendToFile(string msg)
        {
            try
            {
                using (Stream stream = OpenFileOutput("emails.txt", Android.Content.FileCreationMode.Append))
                {
                    if (msg != null && msg.Length != 0)
                    {
                        try
                        {
                            stream.Write(Encoding.ASCII.GetBytes(msg), 0, msg.Length);
                            stream.Close();
                        }
                        catch (Java.IO.IOException e)
                        {
                            e.PrintStackTrace();
                        }
                    }

                    Toast.MakeText(this, "Empty no good", ToastLength.Long).Show();
                }
            }
            catch (Java.IO.FileNotFoundException e)
            {
                e.PrintStackTrace();
            }
        }

        private void writeToTemp(string msg)
        {
            try
            {
                using (Stream stream = OpenFileOutput("emailsTemp.txt", Android.Content.FileCreationMode.WorldWriteable))
                {
                    if (msg != null)
                    {
                        try
                        {
                            stream.Write(Encoding.ASCII.GetBytes(msg), 0, msg.Length);
                            stream.Close();
                        }
                        catch (Java.IO.IOException e)
                        {
                            e.PrintStackTrace();
                        }
                    }
                }
            }
            catch (Java.IO.FileNotFoundException e)
            {
                e.PrintStackTrace();
            }
        }

        private void readFromFile(string path)
        {
            try
            {
                using (Stream stream = OpenFileInput(path))
                {
                    try
                    {
                        byte[] buffer = new byte[4096];
                        stream.Read(buffer, 0, buffer.Length);
                        string str = System.Text.Encoding.Default.GetString(buffer);
                        stream.Close();

                        if (str != null || str.Length == 0)
                            this.tv1.Text = str;
                    }
                    catch (Java.IO.IOException e)
                    {
                        e.PrintStackTrace();
                    }
                }
            }
            catch (Java.IO.FileNotFoundException e)
            {
                e.PrintStackTrace();
            }
        }
    }
}