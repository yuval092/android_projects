﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Media;
using Android.Content;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private Button b1, b2, b3, b4, b5, b6, b7;
        MediaPlayer mp;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            b1 = (Button)FindViewById(Resource.Id.b1);
            b2 = (Button)FindViewById(Resource.Id.b2);
            b3 = (Button)FindViewById(Resource.Id.b3);
            b4 = (Button)FindViewById(Resource.Id.b4);
            b5 = (Button)FindViewById(Resource.Id.b5);
            b6 = (Button)FindViewById(Resource.Id.b6);
            b7 = (Button)FindViewById(Resource.Id.b7);


            b1.Click += B1_Click;
            b2.Click += B2_Click;
            b3.Click += B3_Click;
            b4.Click += B4_Click;
            b5.Click += B5_Click;
            b6.Click += B6_Click;
            b7.Click += B7_Click;
        }

        private void B7_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.c);
            mp.Start();
        }

        private void B6_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.d);
            mp.Start();

        }

        private void B5_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.e);
            mp.Start();
        }

        private void B4_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.f);
            mp.Start();
        }

        private void B3_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.g);
            mp.Start();
        }

        private void B2_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.a);
            mp.Start();
        }


        private void B1_Click(object sender, System.EventArgs e)
        {
            mp = MediaPlayer.Create(this, Resource.Raw.b);
            mp.Start();
        }
    }
}