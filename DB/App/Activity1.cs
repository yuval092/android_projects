﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    [Activity(Label = "Activity1")]
    public class Activity1 : Activity
    {
        EditText fname, lname, grade;
        Button btn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layout1);

            this.fname = (EditText)FindViewById(Resource.Id.fname);
            this.lname = (EditText)FindViewById(Resource.Id.lname);
            this.grade = (EditText)FindViewById(Resource.Id.grade);
            this.btn = (Button)FindViewById(Resource.Id.btn);

            this.btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent();
            intent.PutExtra("fname", this.fname.Text);
            intent.PutExtra("lname", this.lname.Text);
            intent.PutExtra("grade", this.grade.Text);
            SetResult(Result.Ok, intent);
            Finish();
        }
    }
}