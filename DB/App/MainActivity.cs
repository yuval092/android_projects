﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using SQLite;
using System.Collections.Generic;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button bt1;
        Button bt2;
        Button bt3;
        Button bt4;
        TextView tv1;
        EditText et1;
        EditText et2;
        EditText et3;

        int count;
        string path;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            bt1 = (Button)FindViewById(Resource.Id.bt1);
            bt2 = (Button)FindViewById(Resource.Id.bt2);
            bt3 = (Button)FindViewById(Resource.Id.bt3);
            bt4 = (Button)FindViewById(Resource.Id.bt4);
            tv1 = (TextView)FindViewById(Resource.Id.tv1);
            et1 = (EditText)FindViewById(Resource.Id.et1);
            et2 = (EditText)FindViewById(Resource.Id.et2);
            et3 = (EditText)FindViewById(Resource.Id.et3);

            this.count = 0;
            this.path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "meow");

            this.bt1.Click += Bt1_Click;
            this.bt2.Click += Bt2_Click;
            this.bt3.Click += Bt3_Click;
            this.bt4.Click += Bt4_Click;
        }

        private void Bt4_Click(object sender, System.EventArgs e)
        {
            List<Person> persons = getAllPerson();
            foreach(var person in persons)
            {
                Toast.MakeText(this, person.fname, ToastLength.Long).Show();
            }
        }

        private void Bt3_Click(object sender, System.EventArgs e)
        {
            List<Person> allPerson = getAllPerson();
            var db = new SQLiteConnection(path);
            db.Delete<Person>(allPerson[0].id);
        }

        private void Bt2_Click(object sender, System.EventArgs e)
        {
            List<Person> persons = getAllPerson();

            persons[0].setPerson("Very", "Secret", 12);
            var db = new SQLiteConnection(path);
            db.Update(persons[0]);
            Toast.MakeText(this, "Updated database", ToastLength.Long).Show();
        }

        private void Bt1_Click(object sender, System.EventArgs e)
        {
            var db = new SQLiteConnection(path);
            db.CreateTable<Person>();
            Person person = new Person("Yuval", "Farkash", count);
            db.Insert(person);
            this.count++;
        }

        public List<Person> getAllPerson()
        {
            List<Person> personList = new List<Person>();
            var db = new SQLiteConnection(path);
            string strsql = string.Format("SELECT * FROM persons");
            var persons = db.Query<Person>(strsql);
            personList = new List<Person>();
            if (persons.Count > 0)
            {
                foreach (var item in persons)
                {
                    personList.Add(item);
                }
            }
            return personList;
        }
    }
}