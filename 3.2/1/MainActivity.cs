﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace _1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView thinPic;
        ImageView normalPic;
        ImageView fatPic;
        SeekBar bar1;
        SeekBar bar2;
        SeekBar bar3;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            thinPic = (ImageView)FindViewById(Resource.Id.thinPic);
            normalPic = (ImageView)FindViewById(Resource.Id.normalPic);
            fatPic = (ImageView)FindViewById(Resource.Id.fatPic);
            bar1 = (SeekBar)FindViewById(Resource.Id.bar1);
            bar2 = (SeekBar)FindViewById(Resource.Id.bar2);
            bar3 = (SeekBar)FindViewById(Resource.Id.bar3);

            bar1.ProgressChanged += Bar1_ProgressChanged;
            bar2.ProgressChanged += Bar2_ProgressChanged;
            bar3.ProgressChanged += Bar3_ProgressChanged;
        }

        private void Bar1_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            thinPic.Alpha = e.Progress;
        }
        private void Bar2_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            normalPic.Alpha = e.Progress;
        }
        private void Bar3_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            fatPic.Alpha = e.Progress;
        }
    }
}