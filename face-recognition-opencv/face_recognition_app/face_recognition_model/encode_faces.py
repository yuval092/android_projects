from imutils import paths
import face_recognition
from PIL import Image
import argparse
import base64
import pickle
import cv2
import os

DETECTION_METHOD = 'hog'
ENCODINGS_FILE_PATH = r"face_recognition_app\face_recognition_model\encodings.pickle"
DATASET_PATH = r"face_recognition_app\face_recognition_model\dataset"

def add_to_dataset(data):
	# lol directory traversl...
	for key in data:
		path = "{0}\\{1}".format(DATASET_PATH, key)
		if not os.path.isdir(path):
			os.mkdir(path)

		i = 1
		for img_data in data[key]:
			img_path = "{0}\\img{1}.png".format(path, i)
			with open(img_path, "wb") as f:
				f.write(base64.b64decode(img_data))
			
			img = Image.open(img_path)
			img.rotate(90, expand=1).save(img_path)
			i += 1

def encode_dataset():
	# grab the paths to the input images in our dataset
	print("[INFO] quantifying faces...")
	imagePaths = list(paths.list_images(DATASET_PATH))

	# initialize the list of known encodings and known names
	knownEncodings = []
	knownNames = []

	# loop over the image paths
	for (i, imagePath) in enumerate(imagePaths):
		# extract the person name from the image path
		print("[INFO] processing image {}/{}".format(i + 1,
			len(imagePaths)))
		name = imagePath.split(os.path.sep)[-2]

		# load the input image and convert it from RGB (OpenCV ordering)
		# to dlib ordering (RGB)
		image = cv2.imread(imagePath)
		rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input image
		boxes = face_recognition.face_locations(rgb,
			model=DETECTION_METHOD)

		# compute the facial embedding for the face
		encodings = face_recognition.face_encodings(rgb, boxes)

		# loop over the encodings
		for encoding in encodings:
			# add each encoding + name to our set of known names and
			# encodings
			knownEncodings.append(encoding)
			knownNames.append(name)

	# dump the facial encodings + names to disk
	print("[INFO] serializing encodings...")
	data = {"encodings": knownEncodings, "names": knownNames}
	f = open(ENCODINGS_FILE_PATH, "wb")
	f.write(pickle.dumps(data))
	f.close()