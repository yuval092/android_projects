import face_recognition
from PIL import Image
import pickle
import cv2
import json

DETECTION_METHOD = 'hog'
TEMP_FILE_PATH = r"face_recognition_app\face_recognition_model\temp.png"
RESULT_FILE_PATH = r"face_recognition_app\face_recognition_model\result.png"
ENCODINGS_FILE_PATH = r"face_recognition_app\face_recognition_model\encodings.pickle"

def rotate_image_left():
	img = Image.open(TEMP_FILE_PATH)
	img.rotate(90, expand=1).save(TEMP_FILE_PATH)
	
	return cv2.imread(TEMP_FILE_PATH)

def prepare_image(img_data):
	# save img_data to a temp file
	with open(TEMP_FILE_PATH, 'wb') as f:
		f.write(img_data)

	# load the input image and convert it from BGR to RGB
	image = rotate_image_left()

	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
	return image, rgb


def get_image_names(rgb, data):
	
	# detect the (x, y)-coordinates of the bounding boxes corresponding
	# to each face in the input image, then compute the facial embeddings
	# for each face
	print("[INFO] recognizing faces...")
	boxes = face_recognition.face_locations(rgb, model=DETECTION_METHOD)
	encodings = face_recognition.face_encodings(rgb, boxes)

	# initialize the list of names for each face detected
	names = []

	# loop over the facial embeddings
	for encoding in encodings:
		# attempt to match each face in the input image to our known
		# encodings
		matches = face_recognition.compare_faces(data["encodings"],
			encoding)
		name = "Unknown"

		# check to see if we have found a match
		if True in matches:
			# find the indexes of all matched faces then initialize a
			# dictionary to count the total number of times each face
			# was matched
			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}

			# loop over the matched indexes and maintain a count for
			# each recognized face face
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1

			# determine the recognized face with the largest number of
			# votes (note: in the event of an unlikely tie Python will
			# select first entry in the dictionary)
			name = max(counts, key=counts.get)
		
		# update the list of names
		names.append(name)
	return boxes, names

def mark_faces_in_image(image, boxes, names, result_path):
	# loop over the recognized faces
	for ((top, right, bottom, left), name) in zip(boxes, names):
		# draw the predicted face name on the image
		cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
		y = top - 15 if top - 15 > 15 else top + 15
		cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
			0.75, (0, 255, 0), 2)

	# save the output image
	cv2.imwrite(result_path, image)

	return open(RESULT_FILE_PATH, 'rb').read()



def recongize_faces(img_data, encodings_path=ENCODINGS_FILE_PATH, detection_method=DETECTION_METHOD, result_path=RESULT_FILE_PATH):
	
	# load the known faces and embeddings
	print("[INFO] loading encodings...")
	data = pickle.loads(open(encodings_path, "rb").read())

	real_image, rgb_image = prepare_image(img_data)
	boxes, names = get_image_names(rgb_image, data)
	result_image = mark_faces_in_image(real_image, boxes, names, result_path)

	return names, result_image	# result picture will be achieved later