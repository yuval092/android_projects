from django.apps import AppConfig


class FaceRecognitionAppConfig(AppConfig):
    name = 'face_recognition_app'
