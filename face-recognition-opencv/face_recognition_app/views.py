from django.http import HttpResponse, JsonResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from face_recognition_app.face_recognition_model.recognize_faces_image import recongize_faces
from face_recognition_app.face_recognition_model.encode_faces import add_to_dataset, encode_dataset
import base64
import json

@csrf_exempt
def recognize(request):
    if request.method != 'POST':
        return HttpResponse("Bad request", 400)
    
    try:
        data = json.loads(request.body.decode())
        
        img_data = base64.b64decode(data["img_data"])
        names, marked_image = recongize_faces(img_data)
        marked_image = base64.b64encode(marked_image).decode()

        data = {
            "names": names,
            "marked_image": marked_image
        }

    except Exception as e:
        return HttpResponse(e, 500)
    
    return JsonResponse(data)


@csrf_exempt
def add(request):
    if request.method != 'POST':
        return HttpResponse("Bad request", 400)

    try:
        data = json.loads(request.body.decode())
        add_to_dataset(data["students_images"])
        
    except:
        return HttpResponse(0, 500)

    return HttpResponse(1, 200)

@csrf_exempt
def encode(request):
    if request.method != 'GET':
        return HttpResponse("Bad request", 400)

    try:
        encode_dataset()
    except:
        return HttpResponse(0, 500)

    return HttpResponse(1, 200)
