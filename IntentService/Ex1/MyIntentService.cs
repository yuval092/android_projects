﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    [Service]
    class MyIntentService : IntentService
    {
        MyHandler myHandler;
        int counter;

        public MyIntentService():base("DemoIntentService")
        {
            this.myHandler = new MyHandler(this);
        }
        protected override void OnHandleIntent(Intent intent)
        {
            this.counter = intent.GetIntExtra("counter", 3);
            Toast.MakeText(this, "Service started", ToastLength.Short).Show();

            while (this.counter > 0)
            {
                Thread.Sleep(1000);
                Message msg = new Message();
                msg.Arg1 = this.counter;
                if (msg.Arg1 == 0)
                    break;
                this.myHandler.SendMessage(msg);
                this.counter--;
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            Toast.MakeText(this, "Service Stopped", ToastLength.Short).Show();
        }
    }
}