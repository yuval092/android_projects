﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    [Activity(Label = "MainActivity2")]
    public class MainActivity2 : Activity
    {
        TextView txt;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main2);

            this.txt = (TextView)FindViewById(Resource.Id.tv);

            this.txt.Text = "Welcome " + Intent.GetStringExtra("firstName") + " " + Intent.GetStringExtra("lastName");
        }
    }
}