﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText first;
        EditText last;
        Button btn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.first = (EditText)FindViewById(Resource.Id.etFname);
            this.last = (EditText)FindViewById(Resource.Id.etLname);
            this.btn = (Button)FindViewById(Resource.Id.btnSubmit);

            this.btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
            string f = this.first.Text;
            string l = this.last.Text;

            Intent intent = new Intent(this, typeof(MainActivity2));
            intent.PutExtra("firstName", f);
            intent.PutExtra("lastName", l);
            StartActivity(intent);
        }
    }
}