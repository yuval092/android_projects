﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        LinearLayout main;
        ImageView iv;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            //loadPicsHorizontal(5);
            loadPicsLinear(5);
        }

        public void loadPicsHorizontal(int n)
        {
            int imageKey = 0, ind = 0;

            this.main = (LinearLayout)FindViewById(Resource.Id.l1);
            HorizontalScrollView scr = new HorizontalScrollView(this);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

            scr.LayoutParameters = lp;

            LinearLayout linIn = new LinearLayout(this);
            linIn.LayoutParameters = lp;

            for(int i = 1; i <= n; ++i)
            {
                ind = i % 10;
                iv = new ImageView(this);
                LinearLayout.LayoutParams ivParams = new LinearLayout.LayoutParams(400, 400);
                ivParams.SetMargins(10, 20, 0, 0);
                iv.LayoutParameters = ivParams;

                imageKey = Resources.GetIdentifier("d" + i, "drawable", this.PackageName);
                iv.SetImageResource(imageKey);
                linIn.AddView(iv);
            }
            scr.AddView(linIn);
            this.main.AddView(scr);
        }

        public void loadPicsLinear(int n)
        {
            int imageKey = 0, ind = 0;

            this.main = (LinearLayout)FindViewById(Resource.Id.l1);
            ScrollView scr = new ScrollView(this);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

            scr.LayoutParameters = lp;

            LinearLayout linIn = new LinearLayout(this);
            linIn.Orientation = Orientation.Vertical;
            linIn.LayoutParameters = lp;

            for (int i = 1; i <= n; ++i)
            {
                ind = i % 10;
                iv = new ImageView(this);
                LinearLayout.LayoutParams ivParams = new LinearLayout.LayoutParams(400, 400);
                ivParams.SetMargins(10, 20, 0, 0);
                iv.LayoutParameters = ivParams;

                imageKey = Resources.GetIdentifier("d" + i, "drawable", this.PackageName);
                iv.SetImageResource(imageKey);
                linIn.AddView(iv);
            }
            scr.AddView(linIn);
            this.main.AddView(scr);
        }
    }
}