﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    public class MyHandler : Handler
    {
        Context context;
        
        public MyHandler(Context context)
        {
            this.context = context;
        }

        public override void HandleMessage(Message msg)
        {
            Toast.MakeText(this.context, "Counter: " + msg.Arg1, ToastLength.Long).Show();
        }
    }
}