﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button start, stop;
        Intent intent;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.start = (Button)FindViewById(Resource.Id.start);
            this.stop = (Button)FindViewById(Resource.Id.stop);

            this.start.Click += Start_Click;
            this.stop.Click += Stop_Click;
        }

        private void Start_Click(object sender, System.EventArgs e)
        {
            this.intent = new Intent(this, typeof(MyService));
            this.intent.PutExtra("counter", 15);
            StartService(this.intent);
        }

        private void Stop_Click(object sender, System.EventArgs e)
        {
            StopService(this.intent);
        }
    }
}