﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Ex1
{
    [Service]
    public class MyService : Service
    {
        MyHandler h;
        int counter;

        public override void OnCreate()
        {
            base.OnCreate();

            this.h = new MyHandler(this);
        }
        
        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            this.counter = intent.GetIntExtra("counter", 3);
            Toast.MakeText(this, "Service started", ToastLength.Short).Show();
            Thread t = new Thread(Run);
            t.Start();

            return 0;
        }

        private void Run()
        {
            while (this.counter > 0)
            {
                Thread.Sleep(1000);
                Message msg = new Message();
                msg.Arg1 = this.counter;
                if (msg.Arg1 == 0)
                    break;
                this.h.SendMessage(msg);
                this.counter--;
            }

            StopSelf();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            Toast.MakeText(this, "Service stopped", ToastLength.Short).Show();
            this.counter = 0;
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }
    }
}