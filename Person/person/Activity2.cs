﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace person
{
    [Activity(Label = "Activity2")]
    public class Activity2 : Activity
    {
        EditText et1;
        Button bt1, bt2, bt3;
        TextView tv1, tv2, tv3;
        LinearLayout ll;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layout2);
            // Create your application here
            bt1 = (Button)FindViewById(Resource.Id.bt1);
            bt2 = (Button)FindViewById(Resource.Id.bt2);
            bt3 = (Button)FindViewById(Resource.Id.bt3);
            tv1 = (TextView)FindViewById(Resource.Id.tv1);
            tv2 = (TextView)FindViewById(Resource.Id.tv2);
            tv3 = (TextView)FindViewById(Resource.Id.tv3);
            et1 = (EditText)FindViewById(Resource.Id.et1);
            ll = (LinearLayout)FindViewById(Resource.Id.ll);

            bt1.Click += Bt1_Click;
            bt2.Click += Bt2_Click;
            bt3.Click += Bt3_Click;
        }

        private void Bt3_Click(object sender, EventArgs e)
        {
            try
            {
                this.ll.RemoveAllViews();
                var db = new SQLiteConnection(Helper.getPath());
                string query = "SELECT * FROM Persons WHERE fname = '" + et1.Text + "'";
                List<Person> personList = db.Query<Person>(query);

                foreach (var person in personList)
                {
                    TextView tv = new TextView(this);
                    LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(400, 100);
                    tv.LayoutParameters = lParams;
                    tv.TextSize = 15;
                    tv.Text = person.id + ": " + person.fname + " " + person.lname + ", " + person.age;
                    ll.AddView(tv);
                }

                this.et1.Text = "";
            }
            catch
            {
                Toast.MakeText(this, "Failed to search by name", ToastLength.Long).Show();
            }
        }

        private void Bt2_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void Bt1_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new SQLiteConnection(Helper.getPath());
                string query = "SELECT * FROM Persons WHERE _id = " + et1.Text;
                List<Person> personList = db.Query<Person>(query);

                tv1.Text = personList[0].fname;
                tv2.Text = personList[0].lname;
                tv3.Text = personList[0].age.ToString();
            }
            catch
            {
                Toast.MakeText(this, "Failed to search", ToastLength.Long).Show();
            }
        }
    }
}