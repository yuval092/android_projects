﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace person
{
        class Helper
        {
            public static string getPath()
            {
                string path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "meow");
                return path;
            }

            public static List<Person> getAllPerson()
            {
                List<Person> personList = new List<Person>();
                var db = new SQLiteConnection(getPath());
                string strsql = string.Format("SELECT * FROM persons");
                var persons = db.Query<Person>(strsql);
                personList = new List<Person>();
                if (persons.Count > 0)
                {
                    foreach (var item in persons)
                    {
                        personList.Add(item);
                    }
                }
                return personList;
            }
        }
}