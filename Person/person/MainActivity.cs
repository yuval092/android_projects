﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.Collections.Generic;
using SQLite;
using Android.Content;
using System;

namespace person
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button bt1, bt2, bt3, bt4, bt5;
        string path;
        int count;
        SQLiteConnection db;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            bt1 = (Button)FindViewById(Resource.Id.bt1);
            bt2 = (Button)FindViewById(Resource.Id.bt2);
            bt3 = (Button)FindViewById(Resource.Id.bt3);
            bt4 = (Button)FindViewById(Resource.Id.bt4);
            bt5 = (Button)FindViewById(Resource.Id.bt5);

            this.path = Helper.getPath();
            this.db = new SQLiteConnection(this.path);
            count = 0;

            bt1.Click += Bt1_Click;
            bt2.Click += Bt2_Click;
            bt3.Click += Bt3_Click;
            bt4.Click += Bt4_Click;
            bt5.Click += Bt5_Click;
        }

        private void Bt5_Click(object sender, EventArgs e)
        {
            try
            {
                Intent intent = new Intent(this, typeof(Activity2));
                StartActivity(intent);
            }
            catch(Exception ex)
            {
                Toast.MakeText(this, "Failed to search: " + ex.Message, ToastLength.Long).Show();
            }
        }

        private void Bt4_Click(object sender, System.EventArgs e)
        {
            List<Person> persons = Helper.getAllPerson();
            foreach (var person in persons)
            {
                Toast.MakeText(this, person.id + ": " + person.fname + ", " + person.lname + ", " + person.age, ToastLength.Long).Show();
            }
        }

        private void Bt3_Click(object sender, System.EventArgs e)
        {
            try
            {
                List<Person> allPerson = Helper.getAllPerson();
                this.db.Delete<Person>(allPerson[0].id);
            }
            catch
            {
                Toast.MakeText(this, "Failed to delete", ToastLength.Long).Show();
            }
        }

        private void Bt2_Click(object sender, System.EventArgs e)
        {
            try
            {
                List<Person> persons = Helper.getAllPerson();

                persons[0].setPerson("Very", "Secret", 12);
                this.db.Update(persons[0]);
                Toast.MakeText(this, "Updated database", ToastLength.Long).Show();
            }
            catch
            {
                Toast.MakeText(this, "Failed to change", ToastLength.Long).Show();
            }
        }

        private void Bt1_Click(object sender, System.EventArgs e)
        {
            try
            {
                Intent intent = new Intent(this, typeof(Activity1));
                StartActivityForResult(intent, 0);
            }
            catch
            {
                Toast.MakeText(this, "Failed to insert", ToastLength.Long).Show();
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 0)
            {
                if (resultCode == Result.Ok)
                {
                    string fname = data.Extras.Get("fname").ToString();
                    string lname = data.Extras.Get("lname").ToString();
                    int age = Int32.Parse(data.Extras.Get("age").ToString());

                    Person person = new Person(fname, lname, age);
                    this.db.CreateTable<Person>();
                    db.Insert(person);
                    count++;
                }
            }
        }
    }
}