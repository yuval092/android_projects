﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    class QuestionList
    {
        private int qNum;
        private Question[] qArr;

        public QuestionList()
        {
            this.qNum = 0;
            this.qArr = new Question[4];
            this.qArr[0] = new Question("1 + 1", "2", "7", "10", "3", 'c', 1);
            this.qArr[1] = new Question("3 + 15", "13", "18", "24", "99", 'c', 2);
            this.qArr[2] = new Question("117 - 11", "89", "7", "106", "2", 'c', 3);
            this.qArr[3] = new Question("4 * 6", "9", "77", "34", "24", 'c', 4);

        }

        public int getQNum()
        {
            return this.qNum;
        }

        public Question[] getQArr()
        {
            return this.qArr;
        }
    }
}