﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    class Question
    {
        private string q;
        private string ans1;
        private string ans2;
        private string ans3;
        private string ans4;
        private char category;
        private int correct;

        public Question(string q, string ans1, string ans2, string ans3, string ans4, char category, int correct)
        {
            this.q = q;
            this.ans1 = ans1;
            this.ans2 = ans2;
            this.ans3 = ans3;
            this.ans4 = ans4;
            this.category = category;
            this.correct = correct;
        }

        public string getQ()
        {
            return this.q;
        }
        public string getAns1()
        {
            return this.ans1;
        }
        public string getAns2()
        {
            return this.ans2;
        }
        public string getAns3()
        {
            return this.ans3;
        }
        public string getAns4()
        {
            return this.ans4;
        }
        public char getCategory()
        {
            return this.category;
        }
        public int getCorrect()
        {
            return this.correct;
        }
    }
}