﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btn;
        TextView score;
        ISharedPreferences sp;
        Button btnMaxScore;
        TextView scoreMaxScore;
        int max = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.btn = (Button)FindViewById(Resource.Id.btn);
            this.score = (TextView)FindViewById(Resource.Id.score);
            this.btnMaxScore = (Button)FindViewById(Resource.Id.btnMaxScore);
            this.scoreMaxScore = (TextView)FindViewById(Resource.Id.scoreMaxScore);

            // creating shared preferences object
            this.sp = this.GetSharedPreferences("data", FileCreationMode.Private);

            this.btn.Click += Btn_Click;
            this.btnMaxScore.Click += BtnMaxScore_Click;
        }

        private void BtnMaxScore_Click(object sender, EventArgs e)
        {
            this.scoreMaxScore.Text = "Score: " + this.sp.GetInt("maxScore", 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 0)
            {
                if (resultCode == Result.Ok)
                {
                    this.score.Text = "Score: " + data.Extras.Get("score");
                    if(Int32.Parse(data.Extras.Get("score").ToString()) > max)
                    {
                        var editor = this.sp.Edit();
                        max = Int32.Parse(data.Extras.Get("score").ToString());
                        editor.PutInt("maxScore", max);
                        editor.Commit();
                    }

                    if (this.btnMaxScore.Visibility == Android.Views.ViewStates.Invisible)
                        this.btnMaxScore.Visibility = Android.Views.ViewStates.Visible;
                }
            }
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
            this.scoreMaxScore.Text = "";
            Intent intent = new Intent(this, typeof(GameActivity));
            StartActivityForResult(intent, 0);
        }
    }
}