﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App
{
    [Activity(Label = "GameActivity")]
    public class GameActivity : Activity
    {
        TextView tvQ;
        Button btna1;
        Button btna2;
        Button btna3;
        Button btna4;
        TextView tvPoints;
        TextView tvStatus;
        TextView tvFinish;

        QuestionList ql;
        Question q;
        int curr = 0, sum = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_game);

            this.tvQ = (TextView)FindViewById(Resource.Id.tvQ);
            this.btna1 = (Button)FindViewById(Resource.Id.btna1);
            this.btna2 = (Button)FindViewById(Resource.Id.btna2);
            this.btna3 = (Button)FindViewById(Resource.Id.btna3);
            this.btna4 = (Button)FindViewById(Resource.Id.btna4);
            this.tvPoints = (TextView)FindViewById(Resource.Id.tvPoints);
            this.tvStatus = (TextView)FindViewById(Resource.Id.tvStatus);
            this.tvFinish = (TextView)FindViewById(Resource.Id.tvFinish);

            this.ql = new QuestionList();        // questions creation is in the c'tor.
            nextQuestion();

            this.btna1.Click += Btna1_Click;
            this.btna2.Click += Btna2_Click;
            this.btna3.Click += Btna3_Click;
            this.btna4.Click += Btna4_Click;
            this.tvFinish.Click += TvFinish_Click;
        }

        private void TvFinish_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent();
            intent.PutExtra("score", sum);
            SetResult(Result.Ok, intent);
            Finish();
        }

        private void Btna4_Click(object sender, EventArgs e)
        {
            if (curr > 4 || sum >= 100)
                return;

            if (this.q.getCorrect() == 4)
                sum += 25;
            this.tvPoints.Text = "Points: " + sum.ToString();
            this.tvStatus.Text = "Question number: " + curr.ToString();
            nextQuestion();
        }

        private void Btna3_Click(object sender, EventArgs e)
        {
            if (curr > 4 || sum >= 100)
                return;

            if (this.q.getCorrect() == 3)
                sum += 25;
            this.tvPoints.Text = "Points: " + sum.ToString();
            this.tvStatus.Text = "Question number: " + curr.ToString();
            nextQuestion();
        }

        private void Btna2_Click(object sender, EventArgs e)
        {
            if (curr > 4 || sum >= 100)
                return;

            if (this.q.getCorrect() == 2)
                sum += 25;
            this.tvPoints.Text = "Points: " + sum.ToString();
            this.tvStatus.Text = "Question number: " + curr.ToString();
            nextQuestion();
        }

        private void Btna1_Click(object sender, EventArgs e)
        {
            if (curr > 4 || sum >= 100)
                return;

            if (this.q.getCorrect() == 1)
                sum += 25;
            this.tvPoints.Text = "Points: " + sum.ToString();
            this.tvStatus.Text = "Question number: " + curr.ToString();
            nextQuestion();
        }

        public void nextQuestion()
        {
            if (curr >= 4 || sum >= 100)
            {
                this.tvFinish.Visibility = Android.Views.ViewStates.Visible;
            }
            else
            {
                this.q = this.ql.getQArr()[curr];
                this.tvQ.Text = q.getQ() + " = ?";
                this.btna1.Text = q.getAns1();
                this.btna2.Text = q.getAns2();
                this.btna3.Text = q.getAns3();
                this.btna4.Text = q.getAns4();
                this.tvPoints.Text = "Points: " + sum.ToString();
                this.tvStatus.Text = "Question number: " + (curr + 1).ToString();

                curr++;
            }
        }
    }
}