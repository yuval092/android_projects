﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class HomePage : AppCompatActivity
    {
        ISharedPreferences sp;
        Button friend, machine;
        TextView xWin, oWin, Ties;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HomePageDesign);

            this.friend = (Button)FindViewById(Resource.Id.friend);
            this.machine = (Button)FindViewById(Resource.Id.machine);
            this.xWin = (TextView)FindViewById(Resource.Id.xWin);
            this.oWin = (TextView)FindViewById(Resource.Id.oWin);
            this.Ties = (TextView)FindViewById(Resource.Id.Ties);

            // creating shared preferences object
            this.sp = this.GetSharedPreferences("data", FileCreationMode.Private);

            this.xWin.Text = "X Wins: " + this.sp.GetInt("xWin", 0).ToString();      // I know about GetString...
            this.oWin.Text = "O Wins: " + this.sp.GetInt("oWin", 0).ToString();      // I just want the def val
            this.Ties.Text = "Ties: " + this.sp.GetInt("tWin", 0).ToString();

            this.friend.Click += Friend_Click;
            this.machine.Click += Machine_Click;
        }

        private void Friend_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Game));
            StartActivityForResult(intent, 0);
        }

        private void Machine_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Computer));
            StartActivityForResult(intent, 1);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 0 || requestCode == 1)
            {
                if (resultCode == Result.Ok)
                {
                    this.xWin.Text = "X Wins: " + this.sp.GetInt("xWin", 0).ToString();
                    this.oWin.Text = "O Wins: " + this.sp.GetInt("oWin", 0).ToString();
                    this.Ties.Text = "Ties: " + this.sp.GetInt("tWin", 0).ToString();
                }
                else
                {
                    Toast.MakeText(this, "Oops, something went wrong...", ToastLength.Long).Show();
                }
            }
        }
    }
}