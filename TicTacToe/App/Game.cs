﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = false)]
    public class Game : AppCompatActivity
    {
        Button[,] btns;
        TextView winner;
        Button again, menu;
        ISharedPreferences sp;
        Toast mToast;
        string win = "None";
        int winCount = 0;
        bool curr = false;          // false = X
                                    // true = O

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.GameDesign);

            this.btns = new Button[3, 3];
            this.btns[0, 0] = (Button)FindViewById(Resource.Id.btn1);
            this.btns[0, 1] = (Button)FindViewById(Resource.Id.btn2);
            this.btns[0, 2] = (Button)FindViewById(Resource.Id.btn3);
            this.btns[1, 0] = (Button)FindViewById(Resource.Id.btn4);
            this.btns[1, 1] = (Button)FindViewById(Resource.Id.btn5);
            this.btns[1, 2] = (Button)FindViewById(Resource.Id.btn6);
            this.btns[2, 0] = (Button)FindViewById(Resource.Id.btn7);
            this.btns[2, 1] = (Button)FindViewById(Resource.Id.btn8);
            this.btns[2, 2] = (Button)FindViewById(Resource.Id.btn9);

            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    this.btns[i, j].Click += AllPlayBtn_Click;
                }
            }

            // creating shared preferences object
            this.sp = this.GetSharedPreferences("data", FileCreationMode.Private);

            this.winner = (TextView)FindViewById(Resource.Id.winTxt);
            this.again = (Button)FindViewById(Resource.Id.again);
            this.menu = (Button)FindViewById(Resource.Id.menu);

            this.winner.Visibility = Android.Views.ViewStates.Invisible;
            this.again.Visibility = Android.Views.ViewStates.Invisible;

            this.again.Click += Reset;
            this.menu.Click += Menu_Click;
        }

        // return back to the home page
        private void Menu_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent();
            SetResult(Result.Ok, intent);
            removeToast();
            Finish();
        }

        // manage what happens when a button is clicked
        private void AllPlayBtn_Click(object sender, System.EventArgs e)
        {
            Button btn = (Button)sender;
            removeToast();

            if (btn.Text == "X" || btn.Text == "O")     // prevent changing player after first click
            {
                showAToast("Cell already taken !");
                return;
            }
            else if(this.winner.Visibility == Android.Views.ViewStates.Visible)
            {
                showAToast("Want to play again? Click the button !");
                return;
            }

            btn.Text = !this.curr ? "X" : "O";      // change to current player 
            this.curr = !this.curr;                 // switch to the next turn

            IsWin();
        }

        // check if someone won after current turn, or if it's a tie
        private void IsWin()
        {
            for (int i = 0; i < 3; ++i)      // check cols + rows
            {
                if ((btns[0, i].Text == "X" && btns[1, i].Text == "X" && btns[2, i].Text == "X")
                    || btns[i, 0].Text == "X" && btns[i, 1].Text == "X" && btns[i, 2].Text == "X")
                    this.win = "x";
                else if ((btns[0, i].Text == "O" && btns[1, i].Text == "O" && btns[2, i].Text == "O")
                    || btns[i, 0].Text == "O" && btns[i, 1].Text == "O" && btns[i, 2].Text == "O")
                    this.win = "o";
            }

            // check diagonals
            if ((this.btns[0, 0].Text == "X" && this.btns[01, 1].Text == "X" && this.btns[2, 2].Text == "X")
                || this.btns[0, 2].Text == "X" && this.btns[1, 1].Text == "X" && this.btns[2, 0].Text == "X")
                this.win = "x";
            else if ((this.btns[0, 0].Text == "O" && this.btns[1, 1].Text == "O" && this.btns[2, 2].Text == "O")
                || this.btns[0, 2].Text == "O" && this.btns[1, 1].Text == "O" && this.btns[2, 0].Text == "O")
                this.win = "o";

            // check tie
            int count = 0;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    if (btns[i, j].Text == "X" || btns[i, j].Text == "O")
                    {
                        count++;
                    }
                }
            }

            this.win = count == 9 && this.win == "None" ? "t" : this.win;
            if (this.win != "None")
            {
                this.winner.Text = this.win == "t" ? "It's a tie !" : "Winner: " + this.win;

                this.winCount++;
                this.winner.Visibility = Android.Views.ViewStates.Visible;
                this.again.Visibility = Android.Views.ViewStates.Visible;

                // updating results
                var editor = this.sp.Edit();
                editor.PutInt(this.win + "Win", this.sp.GetInt(this.win + "Win", 0) + this.winCount);
                editor.Commit();
            }
        }

        // reset the current data in the game back to default
        private void Reset(object sender, System.EventArgs e)
        {
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    this.btns[i, j].Text = "";
                }
            }

            removeToast();
            this.win = "None";
            this.winCount = 0;
            this.again.Visibility = Android.Views.ViewStates.Invisible;
            this.winner.Visibility = Android.Views.ViewStates.Invisible;
        }

        // show a new toast according to param, and make sure no other toast stands in it's way
        private void showAToast(string message)
        {
            removeToast();
            this.mToast = Toast.MakeText(this, message, ToastLength.Long);
            this.mToast.Show();
        }

        // remove all current toasts
        private void removeToast()
        {
            if (this.mToast != null)
            {
                this.mToast.Cancel();
            }
        }
    }
}