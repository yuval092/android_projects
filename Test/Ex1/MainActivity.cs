﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText txt1;
        Button btn1, btn2;
        Intent intent;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.txt1 = (EditText)FindViewById(Resource.Id.txt1);
            this.btn1 = (Button)FindViewById(Resource.Id.btn1);
            this.btn2 = (Button)FindViewById(Resource.Id.btn2);

            this.btn1.Click += Btn1_Click;
            this.btn2.Click += Btn2_Click;
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            if(this.txt1.Text.Length > 0)
            {
                this.btn1.Text = this.txt1.Text;
            }

            this.intent = new Intent(this, typeof(MyService));
            this.intent.PutExtra("numCounter", int.Parse(this.txt1.Text));
            StartService(this.intent);
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            StopService(this.intent);
            this.btn1.Text = "Start";
        }
    }
}