﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Ex1
{
    [Service]
    public class MyService : Service
    {
        MyHandler h;
        int num, counter;
        Thread t;
        bool stop;

        public override void OnCreate()
        {
            base.OnCreate();

            this.h = new MyHandler(this);
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            this.num = intent.GetIntExtra("numCounter", 0);
            this.counter = 0;
            this.stop = false;

            Toast.MakeText(this, "I'm alive service", ToastLength.Short).Show();
            this.t = new Thread(Run);
            this.t.Start();

            return 0;
        }

        private void Run()
        {
            this.counter = 1;
            Thread.Sleep(1000);

            while (this.counter <= this.num && this.stop == false)
            {
                Message msg = new Message();
                msg.Arg1 = this.counter;
                if (this.counter > this.num)
                    break;
                this.h.SendMessage(msg);
                Thread.Sleep(2000);
                this.counter++;
            }

            StopSelf();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            this.stop = true;
            
            if (this.counter <= this.num)
                Toast.MakeText(this, "Service stopped - the message was shown " + this.counter + " times", ToastLength.Short).Show();
            else
                Toast.MakeText(this, "Service stopped", ToastLength.Short).Show();
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }
    }
}