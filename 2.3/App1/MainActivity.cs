﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace App1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Switch sw;
        ImageView iv;
        SeekBar sb;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            sw = (Switch)FindViewById(Resource.Id.sw);
            iv = (ImageView)FindViewById(Resource.Id.iv);
            sb = (SeekBar)FindViewById(Resource.Id.sb);

            sw.CheckedChange += Sw_CheckedChange;
            sb.ProgressChanged += Sb_ProgressChanged;
        }

        private void Sw_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
           if(e.IsChecked)
            {
                iv.Visibility = Android.Views.ViewStates.Visible;
            }
            else
            {
                iv.Visibility = Android.Views.ViewStates.Invisible;
            }
        }

        private void Sb_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            float a = (float)e.Progress / 100;
            iv.Alpha = a;
        }
    }
}