﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Media;
using Android.Content;

namespace Ex2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button start, stop;
        SeekBar seek;
        MediaPlayer mp;
        AudioManager am;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.start = (Button)FindViewById(Resource.Id.start);
            this.stop = (Button)FindViewById(Resource.Id.stop);
            this.seek = (SeekBar)FindViewById(Resource.Id.seek);

            this.start.Click += Start_Click;
            this.stop.Click += Stop_Click;
            this.seek.ProgressChanged += Seek_ProgressChanged;
        }

        private void Stop_Click(object sender, System.EventArgs e)
        {
            this.mp.Stop();
        }

        private void Seek_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            this.am.SetStreamVolume(Stream.Music, e.Progress, VolumeNotificationFlags.PlaySound);
        }

        private void Start_Click(object sender, System.EventArgs e)
        {
            this.mp = MediaPlayer.Create(this, Resource.Raw.y2mate);
            this.mp.Start();
            this.am = (AudioManager)GetSystemService(Context.AudioService);
            this.seek.Max = am.GetStreamMaxVolume(Stream.Music);
            this.am.SetStreamVolume(Stream.Music, this.seek.Max / 2, 0);
        }
    }
}