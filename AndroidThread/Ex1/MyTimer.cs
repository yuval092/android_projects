﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Threading;

namespace Ex1
{
    public class MyTimer
    {
        int counter;
        Handler handler;
        public bool stop;

        public MyTimer(Handler handler, int counter)
        {
            this.handler = handler;
            this.counter = counter;
        }

        public void Begin()
        {
            Thread t = new Thread(new ThreadStart(Run));
            this.stop = false;
            t.Start();
        }

        private void Run()
        {
            while(this.counter > 0)
            {
                if(!this.stop)
                {
                    this.counter--;
                    Thread.Sleep(1000);
                    Message msg = new Message();
                    handler.SendMessage(msg);
                }
            }
        }
    }
}