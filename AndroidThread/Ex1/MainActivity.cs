﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace Ex1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        MyHandler myHandler;
        Button btnStop;
        MyTimer timer;
        ProgressBar prg;
        TextView txt;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            this.prg = (ProgressBar)FindViewById(Resource.Id.pb);
            this.btnStop = (Button)FindViewById(Resource.Id.btn);
            this.txt = (TextView)FindViewById(Resource.Id.txt);

            myHandler = new MyHandler(this, this.prg, this.txt);

            timer = new MyTimer(myHandler, 100);
            timer.Begin();

            this.btnStop.Click += BtnStop_Click;
        }


        private void BtnStop_Click(object sender, System.EventArgs e)
        {
            this.timer.stop = !this.timer.stop;
            if (this.timer.stop)
                this.btnStop.Text = "Continue";
            else
                this.btnStop.Text = "Stop";
        }
    }
}