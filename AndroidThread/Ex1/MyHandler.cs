﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ex1
{
    public class MyHandler : Handler
    {
        Context context;
        ProgressBar prg;
        TextView txt;

        public MyHandler(Context context, ProgressBar prg, TextView txt)
        {
            this.context = context;
            this.prg = prg;
            this.txt = txt;
        }

        public override void HandleMessage(Message msg)
        {
            this.prg.Progress += 1;
            this.txt.Text = this.prg.Progress.ToString();
        }
    }
}